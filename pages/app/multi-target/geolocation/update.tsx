import Layout from "@common/Layout";
import { changeToastAction } from "Components/slices/utils/reducer";
import { useGetGeoLocation, useUpdateGeoLocation } from "Hooks/useGeoLocation";
import { useGetGroups } from "Hooks/useGroup";
import dynamic from "next/dynamic";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { ReactElement, useEffect, useState } from "react";
import { Button, Card, Col, Container, Form, Row } from "react-bootstrap";
import { useDispatch } from "react-redux";

const Select = dynamic(() => import("react-select"), { ssr: false });

const intervalOptions: any = [
    { value: 15, label: "15 minutes" },
    { value: 30, label: "30 minutes" },
    { value: 60, label: "60 minutes" },
];

const UpdateGeoLocation = () => {
    const router = useRouter();
    const dispatch = useDispatch();

    const { id } = router.query;

    const { data } = useGetGroups();
    const geo = useGetGeoLocation();
    const { mutateAsync } = useUpdateGeoLocation();

    const dataForEdit =
        geo?.data && geo?.data?.filter((item: any) => item.id === id)?.[0];

    const [group, setGroup] = useState<any>({});
    const [interval, setInterval] = useState<number>();

    const [isLoading, setIsLoading] = useState(false);

    const groupOptions =
        data &&
        data.map((item: any) => ({
            value: item.id,
            label: item.groupName,
        }));

    useEffect(() => {
        if (dataForEdit) {
            setGroup({
                groupId: dataForEdit.id,
                groupName: dataForEdit.groupName,
            });
            setInterval(dataForEdit.jobInterval);
        }
    }, [dataForEdit]);

    const handleGroupChange = (value: object) => {
        setGroup(value);
    };

    const handleIntervalChange = (value: number) => {
        setInterval(value);
    };

    const onSubmit = async () => {
        setIsLoading(true);
        try {
            const dataForSubmit = {
                id,
                groupId: group.groupId,
                groupName: group.groupName,
                jobInterval: interval,
                status: dataForEdit.status,
            };

            const response: any = await mutateAsync(dataForSubmit);

            if (response) {
                setTimeout(() => {
                    setIsLoading(false);
                    dispatch(changeToastAction("update"));
                    router.push("/app/multi-target/geolocation");
                }, 1000);
            }
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <React.Fragment>
            <Head>
                <title>Multi Target | 0320 Xplorer</title>
            </Head>
            <div className="page-content">
                <Container fluid>
                    <Row>
                        <Col lg={12}>
                            <Card>
                                <Card.Header>
                                    <h5 className="card-title mb-0">
                                        Update GeoLocation
                                    </h5>
                                </Card.Header>
                                <Card.Body>
                                    {dataForEdit && (
                                        <>
                                            <Row>
                                                <Col lg={4} className="mb-4">
                                                    <Form.Label>
                                                        Select Group
                                                    </Form.Label>
                                                    <Select
                                                        name="group"
                                                        className="basic-single disabled"
                                                        classNamePrefix="select"
                                                        defaultValue="Select Group"
                                                        options={groupOptions}
                                                        defaultInputValue={
                                                            dataForEdit?.groupName ||
                                                            ""
                                                        }
                                                        onChange={(e: any) =>
                                                            handleGroupChange({
                                                                groupId:
                                                                    e.value,
                                                                groupName:
                                                                    e.label,
                                                            })
                                                        }
                                                    />
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col lg={4} className="mb-4">
                                                    <Form.Label>
                                                        Select Interval
                                                    </Form.Label>
                                                    <Select
                                                        name="interval"
                                                        className="basic-single"
                                                        classNamePrefix="select"
                                                        defaultValue="Select Interval"
                                                        options={
                                                            intervalOptions
                                                        }
                                                        defaultInputValue={
                                                            `${dataForEdit.jobInterval} minutes` ||
                                                            ""
                                                        }
                                                        onChange={(e: any) =>
                                                            handleIntervalChange(
                                                                e.value
                                                            )
                                                        }
                                                    />
                                                </Col>
                                            </Row>
                                        </>
                                    )}
                                    <Row>
                                        <Col>
                                            <Button
                                                className="btn-primary mt-3"
                                                onClick={onSubmit}
                                            >
                                                Update GeoLocation
                                            </Button>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Container>

                {isLoading && (
                    <div
                        style={{
                            background: "rgba(0, 0, 0, 0.5)",
                            position: "absolute",
                            top: 0,
                            bottom: 0,
                            left: 0,
                            right: 0,
                            zIndex: 9999999,
                        }}
                    >
                        <div id="status">
                            <div className="spinner-border text-primary avatar-sm">
                                <span className="visually-hidden">
                                    Loading...
                                </span>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        </React.Fragment>
    );
};

UpdateGeoLocation.getLayout = (page: ReactElement) => {
    return <Layout>{page}</Layout>;
};

export default UpdateGeoLocation;
