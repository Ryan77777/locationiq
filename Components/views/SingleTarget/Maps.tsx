import L from "leaflet";
import "leaflet-defaulticon-compatibility";
import "leaflet-defaulticon-compatibility/dist/leaflet-defaulticon-compatibility.css";
import "leaflet.awesome-markers/dist/leaflet.awesome-markers";
import "leaflet.awesome-markers/dist/leaflet.awesome-markers.css";
import "leaflet/dist/leaflet.css";
import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import {
    Circle,
    LayerGroup,
    LayersControl,
    MapContainer,
    Marker,
    Popup,
    TileLayer,
} from "react-leaflet";

const options = {
    bubblingMouseEvents: true,
    color: "#3186cc",
    dashArray: null,
    dashOffset: null,
    fill: true,
    fillColor: "#3186cc",
    fillOpacity: 0.2,
    fillRule: "evenodd",
    lineCap: "round",
    lineJoin: "round",
    opacity: 1.0,
    radius: 200,
    stroke: true,
    weight: 3,
};

const MarkerIcon = L.AwesomeMarkers.icon({
    extraClasses: "fa-rotate-0",
    icon: "signal",
    iconColor: "white",
    markerColor: "red",
    prefix: "fa",
});

const Maps: React.FC<any> = ({ data, isLoading, onClick }) => {
    const isEmptyData = data.length === 0;
    const position = [-6.7017319, 108.3568091];

    return (
        <React.Fragment>
            <Col xl={12}>
                <div id="map" className="details-on-map show-map">
                    <MapContainer
                        center={position}
                        zoom={5}
                        animate={true}
                        zoomControl={false}
                    >
                        <TileLayer
                            attribution='&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>'
                            url="https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_all/{z}/{x}/{y}.png"
                        />

                        {isLoading ? null : !isEmptyData ? (
                            <>
                                <LayersControl position="topright">
                                    <LayersControl.Overlay
                                        checked
                                        name="Marker with popup"
                                    >
                                        <LayerGroup>
                                            {data.map(
                                                (item: any, index: number) => (
                                                    <Marker
                                                        key={index}
                                                        position={{
                                                            lat: item.latitude,
                                                            lng: item.longitude,
                                                        }}
                                                        icon={MarkerIcon}
                                                        eventHandlers={{
                                                            click: () =>
                                                                onClick(index),
                                                        }}
                                                    >
                                                        <Popup>
                                                            <Container>
                                                                <strong>
                                                                    DETAIL
                                                                    INFORMATION
                                                                    TARGET
                                                                </strong>
                                                                <Row>
                                                                    <Col xs={3}>
                                                                        <span>
                                                                            MSISDN
                                                                        </span>
                                                                    </Col>
                                                                    <Col xs={1}>
                                                                        <span>
                                                                            :
                                                                        </span>
                                                                    </Col>
                                                                    <Col xs={8}>
                                                                        <span>
                                                                            {item.msisdn ??
                                                                                "-"}
                                                                        </span>
                                                                    </Col>
                                                                    <Col xs={3}>
                                                                        <span>
                                                                            IMSI
                                                                        </span>
                                                                    </Col>
                                                                    <Col xs={1}>
                                                                        <span>
                                                                            :
                                                                        </span>
                                                                    </Col>
                                                                    <Col xs={8}>
                                                                        <span>
                                                                            {item.imsi ??
                                                                                "-"}
                                                                        </span>
                                                                    </Col>
                                                                    <Col xs={3}>
                                                                        <span>
                                                                            IMEI
                                                                        </span>
                                                                    </Col>
                                                                    <Col xs={1}>
                                                                        <span>
                                                                            :
                                                                        </span>
                                                                    </Col>
                                                                    <Col xs={8}>
                                                                        <span>
                                                                            {item.imei ??
                                                                                "-"}
                                                                        </span>
                                                                    </Col>
                                                                </Row>
                                                                <hr />
                                                                <Row>
                                                                    <Col xs={3}>
                                                                        <span>
                                                                            ADDRESS
                                                                        </span>
                                                                    </Col>
                                                                    <Col xs={1}>
                                                                        <span>
                                                                            :
                                                                        </span>
                                                                    </Col>
                                                                    <Col xs={8}>
                                                                        <span>
                                                                            {item.address ??
                                                                                "-"}
                                                                        </span>
                                                                    </Col>
                                                                    <Col xs={3}>
                                                                        <span>
                                                                            TYPE
                                                                        </span>
                                                                    </Col>
                                                                    <Col xs={1}>
                                                                        <span>
                                                                            :
                                                                        </span>
                                                                    </Col>
                                                                    <Col xs={8}>
                                                                        <span>
                                                                            {item.phone ??
                                                                                "-"}
                                                                        </span>
                                                                    </Col>
                                                                </Row>
                                                            </Container>
                                                        </Popup>
                                                    </Marker>
                                                )
                                            )}
                                        </LayerGroup>
                                    </LayersControl.Overlay>
                                    <LayersControl.Overlay
                                        checked
                                        name="Radius of markers"
                                    >
                                        <LayerGroup>
                                            {data.map(
                                                (item: any, index: number) => (
                                                    <Circle
                                                        key={index}
                                                        center={{
                                                            lat: item.latitude,
                                                            lng: item.longitude,
                                                        }}
                                                        pathOptions={options}
                                                        radius={200}
                                                    />
                                                )
                                            )}
                                        </LayerGroup>
                                    </LayersControl.Overlay>
                                </LayersControl>
                            </>
                        ) : null}
                    </MapContainer>
                </div>
            </Col>
        </React.Fragment>
    );
};

export default React.memo(Maps);

const circleOptions = {
    strokeColor: "#3186cc",
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: "#3186cc",
    fillOpacity: 0.35,
};
