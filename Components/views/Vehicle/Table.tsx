import TableContainer from "@common/TableContainer";
import React, { useMemo } from "react";

const Table: React.FC<any> = ({ data }) => {
    const columns = useMemo(
        () => [
            {
                Header: "No.",
                Cell: ({ row }: any) => row.index + 1,
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "Vehicle Number",
                accessor: "NO_POLISI",
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "Machine Number",
                accessor: "NO_MESIN",
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "Chassis number",
                accessor: "NO_RANGKA",
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "BPKB number",
                accessor: "NO_BPKB",
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "Type",
                accessor: "TIPE_KEND",
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "Color",
                accessor: "WARNA",
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "Brand",
                accessor: "MERK",
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "NIK",
                accessor: "NIK",
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "Owner",
                accessor: "PEMILIK",
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "Address",
                accessor: "ALAMAT",
                disableFilters: true,
                filterable: false,
            },
        ],
        []
    );

    return (
        <React.Fragment>
            <TableContainer
                columns={columns}
                data={data}
                isPagination
                isBordered
                customPageSize={10}
                className="custom-header-css table align-middle table-nowrap"
                tableClass="table-centered align-middle table-nowrap mb-0"
                theadClass="text-muted table-light"
                SearchPlaceholder="Search..."
            />
        </React.Fragment>
    );
};

export default Table;
