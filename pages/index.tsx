import { useProfile } from "Hooks/useProfile";
import { useRouter } from "next/router";
import { useEffect } from "react";

export default function Index() {
    const router = useRouter();
    const { userProfile } = useProfile();

    useEffect(() => {
        if (!userProfile) {
            router.push("/auth/login");
        } else {
            router.push("/app/dashboard");
        }
    }, [router, userProfile]);
}
