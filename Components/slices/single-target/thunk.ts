import axios from "axios";
import { apiError, storeDataAction, setLoading, resetError } from "./reducer";

const BASE_URL = process.env.NEXT_PUBLIC_BASE_URL;

export const trackingUser = (phone: string) => async (dispatch: any) => {
    dispatch(setLoading());
    dispatch(resetError());
    try {
        const AUTH_KEY = JSON.parse(
            localStorage.getItem("authUser") || ""
        ).token;
        const response: any = await axios.get(`${BASE_URL}/cekpos/${phone}`, {
            headers: {
                Authorization: `Bearer ${AUTH_KEY}`,
            },
        });

        const data = response?.respon?.[0];
        if (data && data.latitude && data.longitude) {
            dispatch(storeDataAction(data));
        } else {
            dispatch(apiError("Data not found"));
        }
    } catch (error: any) {
        dispatch(apiError("Data not found"));
    }
};
