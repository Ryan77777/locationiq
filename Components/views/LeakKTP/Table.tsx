import TableContainer from "@common/TableContainer";
import { useRouter } from "next/navigation";
import React, { useMemo } from "react";
import { Button, Col, Row } from "react-bootstrap";
import { useSelector } from "react-redux";

const Table = () => {
    const router = useRouter();

    const { data, loading } = useSelector((state: any) => state.KTP);

    const columns = useMemo(
        () => [
            {
                Header: "No.",
                Cell: ({ row }: any) => row.index + 1,
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "Nama",
                accessor: "nama",
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "NIK",
                accessor: "nik",
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "Alamat",
                accessor: (cellProps: any) => {
                    return (
                        <p>
                            {cellProps.alamat}, {cellProps.kec}, {cellProps.kab}
                            , {cellProps.pro}
                        </p>
                    );
                },
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "Actions",
                disableFilters: true,
                filterable: true,
                accessor: (cellProps: any) => {
                    return (
                        <React.Fragment>
                            <Row>
                                <Col>
                                    <Button
                                        type="button"
                                        variant="primary"
                                        className="btn btn-sm"
                                        style={{
                                            marginRight: "4px",
                                        }}
                                        onClick={() => {
                                            router.push(
                                                `/app/e-ktp/detail/${cellProps.nik}`
                                            );
                                        }}
                                    >
                                        <i className="bi bi-eye"></i>
                                    </Button>
                                </Col>
                            </Row>
                        </React.Fragment>
                    );
                },
            },
        ],
        [router]
    );

    return (
        <React.Fragment>
            <TableContainer
                columns={columns}
                data={data || []}
                isPagination
                isGlobalFilter
                isBordered
                isLoading={loading}
                customPageSize={10}
                className="custom-header-css table align-middle table-nowrap"
                tableClass="table-centered align-middle table-nowrap mb-0"
                theadClass="text-muted table-light"
                SearchPlaceholder="Search..."
            />
        </React.Fragment>
    );
};

export default Table;
