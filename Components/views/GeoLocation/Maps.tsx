import L from "leaflet";
import "leaflet-defaulticon-compatibility";
import "leaflet-defaulticon-compatibility/dist/leaflet-defaulticon-compatibility.css";
import "leaflet.awesome-markers/dist/leaflet.awesome-markers";
import "leaflet.awesome-markers/dist/leaflet.awesome-markers.css";
import "leaflet.smooth_marker_bouncing";
import "leaflet/dist/leaflet.css";
import React, { useEffect } from "react";
import { Col } from "react-bootstrap";

const Maps: React.FC<any> = ({ data }) => {
    useEffect(() => {
        // Inisialisasi peta
        const map = L.map("map", {
            zoomControl: false,
        }).setView([-6.7017319, 108.3568091], 5);

        // Tambahkan tile layer (misalnya dari OpenStreetMap)
        L.tileLayer(
            "https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_all/{z}/{x}/{y}.png",
            {
                attribution:
                    '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>',
            }
        ).addTo(map);

        // Create a LayerGroup for markers
        const markersLayer = L.layerGroup().addTo(map);

        // Create a LayerGroup for circles
        const circlesLayer = L.layerGroup().addTo(map);

        // Create a LayerGroup for polylines
        const polylinesLayer = L.layerGroup().addTo(map);

        // Tambahkan marker sesuai dengan data
        data.forEach((userPath: any) => {
            // Assuming userPath is an array of coordinates
            const userPathCoordinates = userPath.map((pathCoords: any) => [
                pathCoords.lat,
                pathCoords.lng,
            ]);

            // Tambahkan Polyline ke peta
            const polyline = L.polyline(userPathCoordinates, {
                color: "#3186cc",
                weight: 2,
            }).addTo(polylinesLayer);

            map.fitBounds(polyline.getBounds());
            map.setZoom(5);

            userPath.forEach((coords: any, markerIndex: number) => {
                const isLastMarker = markerIndex === userPath.length - 1;

                const marker = L.marker([coords.lat, coords.lng]);

                marker.setBouncingOptions({
                    bounceHeight: 10,
                    bounceSpeed: 64,
                });

                if (isLastMarker) {
                    L.circle([coords.lat, coords.lng], {
                        radius: 200,
                        color: "#3186cc",
                        fillColor: "#3186cc",
                    }).addTo(circlesLayer);
                    marker.addTo(markersLayer).bounce();
                } else {
                    marker.addTo(markersLayer).bounce();
                }
            });

            // Create a LayersControl and add layers
            L.control
                .layers(null, {
                    "Show Markers": markersLayer,
                    "Show Circles": circlesLayer,
                    "Show Polylines": polylinesLayer,
                })
                .addTo(map);
        });
    }, [data]);
    return (
        <React.Fragment>
            <Col xl={12}>
                <div
                    id="map"
                    className="details-on-map show-detail-map"
                    style={{ height: "100vh" }}
                ></div>
            </Col>
        </React.Fragment>
    );
};

export default Maps;
