import Layout from "@common/Layout";
import { useGetGroups } from "Hooks/useGroup";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { ReactElement } from "react";
import { Card, Col, Container, Row } from "react-bootstrap";

const DetailGroup = () => {
    const router = useRouter();

    const { groupId } = router.query;
    const { data } = useGetGroups();

    const dataForDetail =
        data && data?.filter((item: any) => item.id === groupId)?.[0];

    return (
        <React.Fragment>
            <Head>
                <title>Groups | 0320 Xplorer</title>
            </Head>
            <div className="page-content">
                <Container fluid>
                    <Row>
                        <Col lg={12}>
                            <Card>
                                <Card.Header>
                                    <h5 className="card-title mb-0">
                                        Detail Group
                                    </h5>
                                </Card.Header>
                                <Card.Body>
                                    <h5>
                                        Group Name: {dataForDetail?.groupName}
                                    </h5>
                                    <h5 className="card-title my-3">
                                        Member for this Group
                                    </h5>
                                    {dataForDetail?.trackingList?.map(
                                        (item: any, index: number) => (
                                            <>
                                                <Row
                                                    key={index}
                                                    className="align-items-center"
                                                >
                                                    <Col xs={2} md={2}>
                                                        <p>Name</p>
                                                    </Col>
                                                    <Col
                                                        xs={1}
                                                        md={1}
                                                        className="text-center"
                                                    >
                                                        <p>:</p>
                                                    </Col>
                                                    <Col xs={7} md={7}>
                                                        <p>{item.phoneName}</p>
                                                    </Col>
                                                </Row>
                                                <Row
                                                    key={index}
                                                    className="align-items-center"
                                                >
                                                    <Col xs={2} md={2}>
                                                        <p>Phone Number</p>
                                                    </Col>
                                                    <Col
                                                        xs={1}
                                                        md={1}
                                                        className="text-center"
                                                    >
                                                        <p>:</p>
                                                    </Col>
                                                    <Col xs={7} md={7}>
                                                        <p>
                                                            {item.phoneNumber}
                                                        </p>
                                                    </Col>
                                                </Row>
                                            </>
                                        )
                                    )}
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
        </React.Fragment>
    );
};

DetailGroup.getLayout = (page: ReactElement) => {
    return <Layout>{page}</Layout>;
};

export default DetailGroup;
