import {
    UseMutationResult,
    UseQueryResult,
    useMutation,
    useQuery,
} from "@tanstack/react-query";
import axios from "axios";

const BASE_URL = process.env.NEXT_PUBLIC_BASE_URL_AUTH;

export const useGetUser = (): UseQueryResult<any, Error> => {
    return useQuery<any, Error>({
        queryKey: ["users"],
        queryFn: async () => {
            const response: any = await axios.get(`${BASE_URL}/users`);

            return response.data;
        },
        retry: true,
        refetchOnWindowFocus: true,
        refetchOnMount: true,
    });
};

export const useCreateUser = (): UseMutationResult<any, Error, any> => {
    return useMutation({
        mutationFn: async (data) => {
            const response: any = await axios.post(`${BASE_URL}/users`, data);

            return response;
        },
    });
};

export const useUpdateUser = (): UseMutationResult<any, Error, any> => {
    return useMutation({
        mutationFn: async (data) => {
            const response: any = await axios.patch(`${BASE_URL}/users`, data);

            console.log(response);

            return response;
        },
    });
};

export const useDeleteUser = (): UseMutationResult<any, Error, any> => {
    return useMutation({
        mutationFn: async (id: string) => {
            const response: any = await axios.delete(`${BASE_URL}/users/${id}`);

            return response;
        },
    });
};
