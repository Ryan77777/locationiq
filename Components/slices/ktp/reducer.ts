import { createSlice } from "@reduxjs/toolkit";

export const initialState = {
    data: [],
    total_ktp: 0,
    total_data_updated: 0,
    last_updated_at: null,
    loading: true,
    error: "",
};

const ktpSlice = createSlice({
    name: "ktp",
    initialState,
    reducers: {
        apiError(state, action) {
            state.error = action.payload;
            state.loading = false;
        },
        storeDataKTP(state, action) {
            state.data = action.payload;
            state.error = "";
            state.loading = false;
        },
        storeDataKTPStatistic(state, action) {
            state.total_data_updated = action.payload.total_data_updated;
            state.last_updated_at = action.payload.last_updated_at;
            state.error = "";
            state.loading = false;
        },
        storeTotalKTP(state, action) {
            state.total_ktp = action.payload.total_ktp;
            state.error = "";
            state.loading = false;
        },
        setLoading(state, action) {
            state.loading = action.payload;
        },
    },
});

export const {
    apiError,
    storeDataKTP,
    storeDataKTPStatistic,
    storeTotalKTP,
    setLoading,
} = ktpSlice.actions;

export default ktpSlice.reducer;
