import TableContainer from "@common/TableContainer";
import { getAllLogs, getLogsByUser } from "Components/slices/thunk";
import React, { useEffect, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";

const Table = () => {
    const dispatch: any = useDispatch();

    const { logs: data } = useSelector((state: any) => state.Logs);
    const { user } = useSelector((state: any) => state.Login);

    const getData = () => {
        if (user.role === "Admin") {
            dispatch(getAllLogs());
        } else {
            dispatch(getLogsByUser(user.id));
        }
    };

    useEffect(() => {
        getData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const columns = useMemo(
        () => [
            {
                Header: "No.",
                Cell: ({ row }: any) => row.index + 1,
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "User",
                accessor: "userName",
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "Activity",
                accessor: "logHeader",
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "Timestamp",
                accessor: (cellProps: any) => {
                    return (
                        <p>
                            {moment(cellProps.createdAt).format(
                                "MM/DD/YYYY HH:mm"
                            )}
                        </p>
                    );
                },
                disableFilters: true,
                filterable: false,
            },
        ],
        []
    );

    return (
        <React.Fragment>
            <TableContainer
                columns={columns}
                data={data}
                isPagination
                isGlobalFilter
                isBordered
                customPageSize={10}
                className="custom-header-css table align-middle table-nowrap"
                tableClass="table-centered align-middle table-nowrap mb-0"
                theadClass="text-muted table-light"
                SearchPlaceholder="Search..."
            />
        </React.Fragment>
    );
};

export default Table;
