import {
    apiError,
    loginSuccess,
    logoutSuccess,
    reset_login_flag,
} from "./reducer";

const BASE_URL = process.env.NEXT_PUBLIC_BASE_URL_AUTH;

export const loginUser = (user: any, router: any) => async (dispatch: any) => {
    try {
        const response = await fetch(`${BASE_URL}/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(user),
        });

        if (response.status === 200) {
            const data = await response.json();
            localStorage.setItem("authUser", JSON.stringify(data.data));
            dispatch(loginSuccess(data.data));
            router.replace("/app/dashboard");
        } else {
            dispatch(apiError("There is no account in our database."));
        }
    } catch (error: any) {
        dispatch(apiError("There is no account in our database."));
    }
};

export const logoutUser = (router: any) => async (dispatch: any) => {
    try {
        dispatch(logoutSuccess());
        localStorage.clear();
        router.replace("/auth/login");
    } catch (error) {
        console.log(error);
    }
};

export const resetLoginFlag = () => async (dispatch: any) => {
    try {
        const response = dispatch(reset_login_flag());
        return response;
    } catch (error) {
        console.log(error);
    }
};
