import Layout from "@common/Layout";
import { changeToastAction } from "Components/slices/utils/reducer";
import { useGetGroups, useUpdateGroup } from "Hooks/useGroup";
import Cleave from "cleave.js/react";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { ReactElement, useEffect, useState } from "react";
import { Button, Card, Col, Container, Form, Row } from "react-bootstrap";
import { useDispatch } from "react-redux";

const UpdateGroup = () => {
    const router = useRouter();
    const dispatch = useDispatch();

    const { groupId } = router.query;

    const { data } = useGetGroups();
    const { mutateAsync } = useUpdateGroup();

    const dataForEdit =
        data && data?.filter((item: any) => item.id === groupId)?.[0];

    const [groupName, setGroupName] = useState("");
    const [trackingList, setTrackingList] = useState([
        { phoneName: "", phoneNumber: "" },
    ]);

    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        if (dataForEdit) {
            setGroupName(dataForEdit.groupName);
            setTrackingList(dataForEdit.trackingList);
        }
    }, [dataForEdit]);

    const addMember = () => {
        setTrackingList([...trackingList, { phoneName: "", phoneNumber: "" }]);
    };

    const removeMember = (index: number) => {
        const updatedTrackingList = [...trackingList];
        updatedTrackingList.splice(index, 1);
        setTrackingList(updatedTrackingList);
    };

    const handleChange = (value: string) => {
        setGroupName(value);
    };

    const handleNameChange = (index: number, value: string) => {
        const updatedTrackingList = [...trackingList];
        updatedTrackingList[index].phoneName = value;
        setTrackingList(updatedTrackingList);
    };

    const handlePhoneChange = (index: number, value: string) => {
        const updatedTrackingList = [...trackingList];
        updatedTrackingList[index].phoneNumber = value;
        setTrackingList(updatedTrackingList);
    };

    const onSubmit = async () => {
        setIsLoading(true);
        try {
            const data = {
                groupName,
                id: groupId,
                trackingList,
            };
            const response = await mutateAsync(data);

            if (response) {
                setTimeout(() => {
                    setIsLoading(false);
                    dispatch(changeToastAction("update"));
                    router.push("/app/groups");
                }, 1000);
            }
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <React.Fragment>
            <Head>
                <title>Group | 0320 Xplorer</title>
            </Head>
            <div className="page-content">
                <Container fluid>
                    <Row>
                        <Col lg={12}>
                            <Card>
                                <Card.Header>
                                    <h5 className="card-title mb-0">
                                        Update Group
                                    </h5>
                                </Card.Header>
                                <Card.Body>
                                    <Row className="gy-4">
                                        <Col lg={4} className="mb-4">
                                            <Form.Label>Group Name</Form.Label>
                                            <Form.Control
                                                type="text"
                                                placeholder="Input here..."
                                                value={groupName}
                                                onChange={(e) =>
                                                    handleChange(e.target.value)
                                                }
                                            />
                                        </Col>
                                    </Row>
                                    <h5 className="card-title mb-3">
                                        Member for this Group
                                    </h5>
                                    {trackingList.map((member, index) => (
                                        <Row
                                            id={`form-${index}`}
                                            className="align-items-center"
                                            key={index}
                                        >
                                            <Col lg={3} className="mb-4">
                                                <Form.Label>Name</Form.Label>
                                                <Form.Control
                                                    type="text"
                                                    placeholder="Input here..."
                                                    value={member.phoneName}
                                                    onChange={(e) =>
                                                        handleNameChange(
                                                            index,
                                                            e.target.value
                                                        )
                                                    }
                                                />
                                            </Col>
                                            <Col lg={3} className="mb-4">
                                                <Form.Label>
                                                    Phone Number
                                                </Form.Label>
                                                <Cleave
                                                    className="form-control"
                                                    placeholder="xxxx xxx xxx xxx"
                                                    options={{
                                                        blocks: [4, 3, 3, 3],
                                                        delimiter: " ",
                                                    }}
                                                    value={member.phoneNumber}
                                                    onChange={(e: any) =>
                                                        handlePhoneChange(
                                                            index,
                                                            e.target.value
                                                        )
                                                    }
                                                />
                                            </Col>
                                            {index > 0 && (
                                                <Col lg={1}>
                                                    <Button
                                                        variant="danger"
                                                        className="btn btn-sm"
                                                        onClick={() =>
                                                            removeMember(index)
                                                        }
                                                    >
                                                        <i className="ri-delete-bin-5-line"></i>
                                                    </Button>
                                                </Col>
                                            )}
                                        </Row>
                                    ))}
                                    <Button
                                        className="btn-ghost-primary"
                                        onClick={addMember}
                                    >
                                        <span className="d-flex align-content-center">
                                            <i className="ri-add-line"></i>
                                            <span className="flex-grow-1 ms-2">
                                                Add New
                                            </span>
                                        </span>
                                    </Button>
                                    <Row className="mt-5">
                                        <Col>
                                            <Button
                                                className="btn-primary mt-3"
                                                onClick={onSubmit}
                                            >
                                                Update Group
                                            </Button>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Container>

                {isLoading && (
                    <div
                        style={{
                            background: "rgba(0, 0, 0, 0.5)",
                            position: "absolute",
                            top: 0,
                            bottom: 0,
                            left: 0,
                            right: 0,
                            zIndex: 9999999,
                        }}
                    >
                        <div id="status">
                            <div className="spinner-border text-primary avatar-sm">
                                <span className="visually-hidden">
                                    Loading...
                                </span>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        </React.Fragment>
    );
};

UpdateGroup.getLayout = (page: ReactElement) => {
    return <Layout>{page}</Layout>;
};

export default UpdateGroup;
