import Layout from "@common/Layout";
import {
    DrawingManager,
    GoogleMap,
    Polygon,
    useLoadScript,
} from "@react-google-maps/api";
import { changeToastAction } from "Components/slices/utils/reducer";
import { useGetGeoFencing, useUpdateGeoFencing } from "Hooks/useGeoFencing";
import Cleave from "cleave.js/react";
import dynamic from "next/dynamic";
import Head from "next/head";
import { useRouter } from "next/router";
import React, {
    ReactElement,
    useCallback,
    useEffect,
    useMemo,
    useRef,
    useState,
} from "react";
import {
    Button,
    Card,
    Col,
    Container,
    Form,
    Modal,
    Row,
} from "react-bootstrap";
import { useDispatch } from "react-redux";

const Select = dynamic(() => import("react-select"), { ssr: false });

const libraries: any = ["places", "drawing"];

const drawingModes: any = ["rectangle"];

type LatLngLiteral = google.maps.LatLngLiteral;
type MapOptions = google.maps.MapOptions;

const intervalOptions: any = [
    { value: 15, label: "15 minutes" },
    { value: 30, label: "30 minutes" },
    { value: 60, label: "60 minutes" },
];

const notifOptions: any = [
    { value: "IN", label: "IN" },
    { value: "OUT", label: "OUT" },
];

const UpdateGeoFencing = () => {
    const router = useRouter();
    const dispatch = useDispatch();

    const { id } = router.query;

    const mapRef = useRef<any>(null);
    const drawingManagerRef = useRef<any>();

    const { data } = useGetGeoFencing();
    const { mutateAsync } = useUpdateGeoFencing();

    const dataForEdit =
        data && data?.filter((item: any) => item.id === id)?.[0];

    const [isLoading, setIsLoading] = useState(false);
    const [isVisible, setIsVisible] = useState(false);

    // Form State
    const [name, setName] = useState("");
    const [phone, setPhone] = useState("");
    const [interval, setInterval] = useState<number>();
    const [notif, setNotif] = useState("");
    const [rectangles, setRectangles] = useState<any>([]);

    useEffect(() => {
        if (dataForEdit) {
            setName(dataForEdit.phoneName);
            setPhone(dataForEdit.phoneNumber);
            setInterval(dataForEdit.jobInterval);
            setRectangles(dataForEdit.location);
            setNotif(dataForEdit.detectBy);
        }
    }, [dataForEdit]);

    const toggleModal = useCallback(() => {
        setIsVisible(false);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    // Form OnChange
    const handleNameChange = (e: any) => {
        setName(e.target.value);
    };

    const handlePhoneChange = (e: any) => {
        setPhone(e.target.value);
    };

    const handleIntervalChange = (value: number) => {
        setInterval(value);
    };

    const handleNotifChange = (value: string) => {
        setNotif(value);
    };
    // End Form OnChange

    // Submit
    const onSubmit = async () => {
        setIsLoading(true);
        try {
            const dataForSubmit = {
                id,
                phoneNumber: phone,
                phoneName: name,
                jobInterval: interval,
                location: rectangles,
                notifBy: notif,
                status: dataForEdit.status,
            };
            const response: any = await mutateAsync(dataForSubmit);

            if (response) {
                setTimeout(() => {
                    setIsLoading(false);
                    dispatch(changeToastAction("update"));
                    router.push("/app/multi-target/geofencing");
                }, 1000);
            }
        } catch (error) {
            console.log(error);
        }
    };

    // Maps
    const { isLoaded } = useLoadScript({
        googleMapsApiKey: process.env.NEXT_PUBLIC_GOOGLE_MAP_KEY!,
        libraries,
    });

    const center = useMemo<LatLngLiteral>(
        () => ({ lat: -0.7893, lng: 113.9213 }),
        []
    );

    const options = useMemo<MapOptions>(
        () => ({
            mapId: "122be4461fa39403",
            disableDefaultUI: true,
            clickableIcons: false,
        }),
        []
    );

    const polygonOptions = {
        fillOpacity: 0.3,
        fillColor: "#ff0000",
        strokeColor: "#ff0000",
        strokeWeight: 1,
        draggable: true,
        editable: true,
    };

    const drawingManagerOptions = {
        drawingControl: true,
        drawingControlOptions: {
            drawingModes,
        },
    };

    const onLoad = useCallback((map: any) => {
        mapRef.current = map;

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const onUnmount = useCallback((map: any) => {
        mapRef.current = null;
    }, []);

    const onLoadDrawingManager = (drawingManager: any) => {
        drawingManagerRef.current = drawingManager;
    };

    const onOverlayComplete = ($overlayEvent: any) => {
        drawingManagerRef.current.setDrawingMode(null);
        if (
            $overlayEvent.type ===
            window.google.maps.drawing.OverlayType.RECTANGLE
        ) {
            const bounds = $overlayEvent.overlay.getBounds();
            const ne = bounds.getNorthEast();
            const sw = bounds.getSouthWest();
            const newRectangle = [
                {
                    lat: ne.lat(),
                    lng: ne.lng(),
                },
                {
                    lat: sw.lat(),
                    lng: ne.lng(),
                },
                {
                    lat: sw.lat(),
                    lng: sw.lng(),
                },
                {
                    lat: ne.lat(),
                    lng: sw.lng(),
                },
                {
                    lat: ne.lat(),
                    lng: ne.lng(),
                },
            ];

            $overlayEvent.overlay?.setMap(null);
            setRectangles(newRectangle);
        }
    };

    const onDeleteShape = () => {
        setRectangles([]);
    };
    // End Maps

    return (
        <React.Fragment>
            <Head>
                <title>Multi Target | 0320 Xplorer</title>
            </Head>
            <div className="page-content">
                <Container fluid>
                    <Row>
                        <Col lg={12}>
                            <Card>
                                <Card.Header>
                                    <h5 className="card-title mb-0">
                                        Update GeoFencing
                                    </h5>
                                </Card.Header>
                                <Card.Body>
                                    <Row>
                                        <Col lg={4} className="mb-4">
                                            <Form.Label>Name</Form.Label>
                                            <Form.Control
                                                type="text"
                                                placeholder="Input here..."
                                                value={name}
                                                onChange={handleNameChange}
                                            />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col lg={4} className="mb-4">
                                            <Form.Label>
                                                Phone Number
                                            </Form.Label>
                                            <Cleave
                                                className="form-control"
                                                placeholder="xxxx xxx xxx xxx"
                                                options={{
                                                    blocks: [4, 3, 3, 3],
                                                    delimiter: " ",
                                                }}
                                                value={phone}
                                                onChange={handlePhoneChange}
                                            />
                                        </Col>
                                    </Row>
                                    {dataForEdit && (
                                        <Row>
                                            <Col lg={4} className="mb-4">
                                                <Form.Label>
                                                    Select Interval
                                                </Form.Label>
                                                <Select
                                                    name="interval"
                                                    className="basic-single"
                                                    classNamePrefix="select"
                                                    defaultValue="Select Interval"
                                                    options={intervalOptions}
                                                    defaultInputValue={
                                                        `${dataForEdit.jobInterval} minutes` ||
                                                        ""
                                                    }
                                                    onChange={(e: any) =>
                                                        handleIntervalChange(
                                                            e.value
                                                        )
                                                    }
                                                />
                                            </Col>
                                        </Row>
                                    )}
                                    <Row className="align-items-center">
                                        <Col lg={3} className="mb-4">
                                            <Form.Label>Location</Form.Label>
                                            <Form.Control
                                                type="text"
                                                value={rectangles}
                                                disabled
                                            />
                                        </Col>
                                        <Col lg={2}>
                                            <Form.Label></Form.Label>
                                            <Button
                                                className="btn-ghost-primary"
                                                onClick={() =>
                                                    setIsVisible(true)
                                                }
                                            >
                                                <span className="d-flex align-content-center">
                                                    <i className="bi bi-map"></i>
                                                    <span className="flex-grow-1 ms-2">
                                                        Choose Location
                                                    </span>
                                                </span>
                                            </Button>
                                        </Col>
                                    </Row>
                                    {dataForEdit && (
                                        <Row>
                                            <Col lg={4} className="mb-4">
                                                <Form.Label>
                                                    Notif by
                                                </Form.Label>
                                                <Select
                                                    name="notif"
                                                    className="basic-single"
                                                    classNamePrefix="select"
                                                    defaultValue="Select Notif by"
                                                    options={notifOptions}
                                                    defaultInputValue={
                                                        dataForEdit.detectBy ||
                                                        ""
                                                    }
                                                    onChange={(e: any) =>
                                                        handleNotifChange(
                                                            e.value
                                                        )
                                                    }
                                                />
                                            </Col>
                                        </Row>
                                    )}
                                    <Row>
                                        <Col>
                                            <Button
                                                className="btn-primary mt-3"
                                                onClick={onSubmit}
                                            >
                                                Update GeoFencing
                                            </Button>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Container>

                {isLoading && (
                    <div
                        style={{
                            background: "rgba(0, 0, 0, 0.5)",
                            position: "absolute",
                            top: 0,
                            bottom: 0,
                            left: 0,
                            right: 0,
                            zIndex: 9999999,
                        }}
                    >
                        <div id="status">
                            <div className="spinner-border text-primary avatar-sm">
                                <span className="visually-hidden">
                                    Loading...
                                </span>
                            </div>
                        </div>
                    </div>
                )}

                {!isLoaded && <div>Loading...</div>}

                {dataForEdit && isVisible && (
                    <Modal
                        size="xl"
                        show={isVisible}
                        onHide={toggleModal}
                        centered
                    >
                        <Modal.Body className="p-2">
                            <div
                                style={{ height: "80vh", position: "relative" }}
                            >
                                <GoogleMap
                                    ref={mapRef}
                                    mapContainerStyle={{
                                        height: "100%",
                                        width: "100%",
                                    }}
                                    zoom={5}
                                    center={center}
                                    options={options}
                                    onLoad={onLoad}
                                    onUnmount={onUnmount}
                                >
                                    {rectangles?.length === 0 && (
                                        <DrawingManager
                                            onLoad={onLoadDrawingManager}
                                            onOverlayComplete={
                                                onOverlayComplete
                                            }
                                            options={drawingManagerOptions}
                                        />
                                    )}
                                    <Polygon
                                        paths={rectangles}
                                        draggable
                                        editable
                                        options={polygonOptions}
                                    />
                                </GoogleMap>
                                {rectangles?.length > 0 && (
                                    <Button
                                        type="button"
                                        variant="danger"
                                        className="btn btn-sm position-absolute top-0 end-0 m-3"
                                        onClick={onDeleteShape}
                                    >
                                        <i className="bi bi-trash"></i>
                                    </Button>
                                )}
                            </div>
                        </Modal.Body>
                    </Modal>
                )}
            </div>
        </React.Fragment>
    );
};

UpdateGeoFencing.getLayout = (page: ReactElement) => {
    return <Layout>{page}</Layout>;
};

export default UpdateGeoFencing;
