import {
    Circle,
    GoogleMap,
    Marker,
    useLoadScript,
} from "@react-google-maps/api";
import axios from "axios";
import { useRouter } from "next/router";
import React, {
    useCallback,
    useEffect,
    useMemo,
    useRef,
    useState,
} from "react";
import { Button, Card, Col, Row } from "react-bootstrap";
import { LayerGroup } from "react-leaflet";
import { LayersControl } from "react-leaflet";
import { TileLayer } from "react-leaflet";
import { MapContainer } from "react-leaflet";

type LatLngLiteral = google.maps.LatLngLiteral;
type MapOptions = google.maps.MapOptions;

const BASE_URL = process.env.NEXT_PUBLIC_BASE_URL;

function formatDateTime(timestamp: any) {
    const date = new Date(timestamp * 1000);
    const formattedDate = date.toISOString().slice(0, 19).replace("T", " ");
    return formattedDate;
}

const Maps = () => {
    const router = useRouter();

    const { id } = router.query;

    const mapRef = useRef<any>(null);
    const polylineRefs = useRef<google.maps.Polyline[]>([]);
    const markerRefs = useRef<google.maps.Marker[]>([]);

    const [name, setName] = useState<any>("");
    const [data, setData] = useState<any>([]);
    const [detail, setDetail] = useState<any>();

    const [message, setMessage] = useState("Memuat data...");

    // Fetching
    const getData = async () => {
        try {
            const AUTH_KEY = JSON.parse(
                localStorage.getItem("authUser") || ""
            ).token;

            const response: any = await axios.get(
                `${BASE_URL}/geolocation-log/jobId/${id}`,
                {
                    headers: {
                        Authorization: `Bearer ${AUTH_KEY}`,
                    },
                }
            );

            if (response.length > 0) {
                const res = response?.[0];

                const result = res?.data.map((_: any, index: number) =>
                    response?.[0]?.data[index].logs.map((location: any) => ({
                        lat: location.lat,
                        lng: location.lng,
                    }))
                );

                const formattedData = res.data.map((item: any) => ({
                    name: item.phoneName ?? "-",
                    phone: item.phoneId,
                    last_updated: formatDateTime(
                        item.logs.length > 0 ? item.logs[0].dateTime : null
                    ),
                }));
                setName(res.groupName);
                setDetail(formattedData);
                setData(result);
            } else {
                setMessage(
                    "Belum ada data. Anda akan kembali ke halaman sebelumnya dalam 3 detik"
                );
                setTimeout(() => {
                    router.back();
                }, 3000);
            }
        } catch (error) {
            console.log(error);
        }
    };
    //

    const { isLoaded } = useLoadScript({
        googleMapsApiKey: process.env.NEXT_PUBLIC_GOOGLE_MAP_KEY!,
    });

    const center = useMemo<LatLngLiteral>(
        () => ({ lat: -0.7893, lng: 113.9213 }),
        []
    );

    const options = useMemo<MapOptions>(
        () => ({
            mapId: "122be4461fa39403",
            disableDefaultUI: true,
            clickableIcons: false,
        }),
        []
    );

    const animateCircle = (line: google.maps.Polyline) => {
        let count = 0;

        window.setInterval(() => {
            count = (count + 1) % 200;

            const icons = line.get("icons");

            icons[0].offset = count / 2 + "%";
            line.set("icons", icons);
        }, 20);
    };

    const onLoad = useCallback(
        async (map: any) => {
            const AUTH_KEY = JSON.parse(
                localStorage.getItem("authUser") || ""
            ).token;
            const response: any = await axios.get(
                `${BASE_URL}/geolocation-log/jobId/${id}`,
                {
                    headers: {
                        Authorization: `Bearer ${AUTH_KEY}`,
                    },
                }
            );

            const result = response?.[0]?.data.map((item: any, index: number) =>
                response?.[0]?.data[index].logs.map((location: any) => ({
                    lat: location.lat,
                    lng: location.lng,
                }))
            );

            mapRef.current = map;

            const allCoordinates = result.flatMap((userPath: any) =>
                userPath.map((coords: any) => ({
                    lat: coords.lat,
                    lng: coords.lng,
                }))
            );

            const bounds = new window.google.maps.LatLngBounds();
            allCoordinates.forEach((coord: any) => {
                bounds.extend(coord);
            });

            const lineSymbol = {
                path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                strokeColor: "#018CF1",
                scale: 2,
            };

            result.forEach((userPath: any) => {
                const coordinates = userPath.map((coords: any) => ({
                    lat: coords.lat,
                    lng: coords.lng,
                }));

                const polyline = new google.maps.Polyline({
                    path: coordinates,
                    strokeColor: "#3186cc",
                    strokeWeight: 2,
                    icons: [
                        {
                            icon: lineSymbol,
                            offset: "0%",
                        },
                    ],
                });

                if (coordinates.length > 1) {
                    polyline.setMap(map);
                    polylineRefs.current.push(polyline);
                    animateCircle(polyline);
                }
            });

            if (mapRef.current && bounds) {
                mapRef.current.fitBounds(bounds, { padding: 50 });
            }
        },
        [id]
    );

    const onUnmount = useCallback(() => {
        if (polylineRefs.current.length > 0) {
            polylineRefs.current.forEach((polyline) => {
                polyline.setMap(null);
            });
        }
        mapRef.current = null;
    }, []);

    useEffect(() => {
        if (isLoaded && data.length > 0) {
            data.forEach((userPath: any) => {
                userPath?.forEach((coords: any, markerIndex: number) => {
                    const isLastMarker = markerIndex === userPath?.length - 1;

                    const marker = new google.maps.Marker({
                        position: {
                            lat: coords.lat,
                            lng: coords.lng,
                        },
                        icon: {
                            url: isLastMarker
                                ? "https://maps.google.com/mapfiles/ms/icons/green-dot.png"
                                : "https://maps.google.com/mapfiles/ms/icons/red-dot.png",
                        },
                        animation: google.maps.Animation.BOUNCE,
                        map: mapRef.current,
                    });

                    markerRefs.current.push(marker);
                });
            });
        }
    }, [isLoaded, data]);

    useEffect(() => {
        if (id) {
            getData();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [router]);

    return isLoaded && data.length > 0 ? (
        <div style={{ height: "100vh", position: "relative" }}>
            <GoogleMap
                ref={mapRef}
                mapContainerStyle={{ height: "100%", width: "100%" }}
                zoom={5}
                center={center}
                options={options}
                onLoad={onLoad}
                onUnmount={onUnmount}
            >
                {data.map((userPath: any, userIndex: number) =>
                    userPath.map((coords: any, markerIndex: number) => {
                        const isLastMarker =
                            markerIndex === userPath.length - 1;
                        return (
                            <React.Fragment key={`${userIndex}-${markerIndex}`}>
                                {isLastMarker ? (
                                    <>
                                        <Circle
                                            center={{
                                                lat: coords.lat,
                                                lng: coords.lng,
                                            }}
                                            radius={1000}
                                            options={{
                                                strokeColor: "#3186cc",
                                                fillColor: "#3186cc",
                                            }}
                                        />
                                        <Marker
                                            position={{
                                                lat: coords.lat,
                                                lng: coords.lng,
                                            }}
                                            icon={{
                                                url: "https://maps.google.com/mapfiles/ms/icons/red-dot.png",
                                            }}
                                            animation={
                                                window.google.maps.Animation
                                                    .BOUNCE
                                            }
                                        />
                                    </>
                                ) : (
                                    <Marker
                                        position={{
                                            lat: coords.lat,
                                            lng: coords.lng,
                                        }}
                                        icon={{
                                            url: "https://maps.google.com/mapfiles/ms/icons/blue-dot.png",
                                        }}
                                        animation={
                                            window.google.maps.Animation.BOUNCE
                                        }
                                    />
                                )}
                            </React.Fragment>
                        );
                    })
                )}
            </GoogleMap>

            {/* Detail */}
            <Card
                className="card-custom"
                style={{
                    position: "absolute",
                    top: 24,
                    left: 88,
                    right: 24,
                    background: "rgba(0, 0, 0, 0.1)",
                    boxShadow: "0px 0px 10px rgba(255, 255, 255, 0.4)",
                }}
            >
                <Card.Body>
                    <h5 className="text-white">{name}</h5>
                    <hr className="text-white" />
                    {detail?.map((item: any, index: number) => (
                        <div key={index}>
                            <Row>
                                <Col lg={10} s={10} xs={10}>
                                    <span className="text-white text-start">
                                        {item.name} / {item.phone}
                                    </span>
                                    <br />
                                    <span className="text-white text-start">
                                        Last updated: {item.last_updated}
                                    </span>
                                </Col>
                                <Col lg={2} s={2} xs={2}>
                                    <i className="bi bi-geo-alt-fill text-primary"></i>
                                </Col>
                            </Row>
                            <hr className="text-white" />
                        </div>
                    ))}
                </Card.Body>
            </Card>

            <Button
                variant="outline-light"
                onClick={() => router.back()}
                className="btn-icon"
                style={{
                    position: "absolute",
                    top: 24,
                    left: 24,
                    right: 24,
                    background: "rgba(0, 0, 0, 0.1)",
                    boxShadow: "0px 0px 10px rgba(255, 255, 255, 0.4)",
                    borderRadius: 999,
                }}
            >
                <i className="ri-arrow-left-line"></i>
            </Button>
        </div>
    ) : (
        <div
            style={{
                position: "absolute",
                top: "50%",
                left: "50%",
                transform: "translate(-50%, -50%)",
                textAlign: "center",
            }}
        >
            {message}
        </div>
    );
};

function getColorForIndex(index: number) {
    const colors = ["red", "blue", "green", "yellow"];
    return colors[index % colors.length];
}

export default Maps;
