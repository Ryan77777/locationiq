import { createSlice } from "@reduxjs/toolkit";

export const initialState = {
    logs: [],
    error: "",
};

const logsSlice = createSlice({
    name: "logs",
    initialState,
    reducers: {
        apiError(state, action) {
            state.error = action.payload;
        },
        storeLogs(state, action) {
            state.logs = action.payload;
            state.error = "";
        },
    },
});

export const { apiError, storeLogs } = logsSlice.actions;

export default logsSlice.reducer;
