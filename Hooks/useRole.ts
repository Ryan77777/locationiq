import { UseQueryResult, useQuery } from "@tanstack/react-query";
import axios from "axios";

const BASE_URL = process.env.NEXT_PUBLIC_BASE_URL_AUTH;

export const useGetRole = (): UseQueryResult<any, Error> => {
    return useQuery<any, Error>({
        queryKey: ["roles"],
        queryFn: async () => {
            const response: any = await axios.get(`${BASE_URL}/roles`);

            return response.data;
        },
        retry: true,
        refetchOnWindowFocus: true,
        refetchOnMount: true,
    });
};
