export type GetGroupResponse = {};

export type SingleTargetResponse = {
    rate_limiter: string;
    status: number;
    pipe_trunks_priority: string;
    statusCode: number;
    api_licensed_by: string;
    message: string;
    respon: LocationInfo[];
};

type LocationInfo = {
    gmap_url: string;
    phone: string;
    lac: string;
    net_provider: string;
    latitude: string;
    longitude: string;
    tac: string;
    mcc: string;
    antena_azimuth_degree: string;
    aol: string;
    cellid_group: string;
    cgi: string;
    network: string;
    ci: string;
    city: string;
    subdistrict: string;
    dateCreate: string;
    province: string;
    distance: string;
    district: string;
    state: string;
    address: string;
    power_dbm: string;
    frequency: string;
    imei: string;
    imsi: string;
    registered_hlr: string;
    msisdn: string;
    mnc: string;
    enb: string;
};
