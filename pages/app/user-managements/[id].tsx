import Layout from "@common/Layout";
import { useGetGroups } from "Hooks/useGroup";
import { useGetUser } from "Hooks/useUserManagment";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { ReactElement, useEffect, useState } from "react";
import { Card, Col, Container, Row } from "react-bootstrap";

const DetailUser = () => {
	const router = useRouter();

	const userId = router.query.id;
	const { data } = useGetUser();

	const [dataForDetail, setDataForDetail] = useState<any>(undefined);

	useEffect(() => {
		setDataForDetail(
			data && data?.filter((item: any) => item.userId === userId)?.[0]
		);
	}, [data, userId]);

	return (
		<React.Fragment>
			<Head>
				<title>User | 0320 Xplorer</title>
			</Head>
			<div className="page-content">
				<Container fluid>
					<Row>
						<Col lg={12}>
							<Card>
								<Card.Header>
									<h5 className="card-title mb-0">
										Detail User
									</h5>
								</Card.Header>
								<Card.Body>
									<h5>
										Name :{" "}
										<span className="fw-bolder">
											{dataForDetail?.name}
										</span>
									</h5>
									<h5>
										Email :{" "}
										<span className="fw-bolder">
											{dataForDetail?.email}
										</span>
									</h5>
									<h5>
										Username :{" "}
										<span className="fw-bolder">
											{dataForDetail?.username}
										</span>
									</h5>
									<h5>
										Role :{" "}
										<span className="fw-bolder">
											{dataForDetail?.role}
										</span>
									</h5>
									<h5>
										Status :{" "}
										<span className="fw-bolder">
											{dataForDetail?.status
												? "Aktif"
												: "Tidak Aktif"}
										</span>
									</h5>
								</Card.Body>
							</Card>
						</Col>
					</Row>
				</Container>
			</div>
		</React.Fragment>
	);
};

DetailUser.getLayout = (page: ReactElement) => {
	return <Layout>{page}</Layout>;
};

export default DetailUser;
