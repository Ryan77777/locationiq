import { createSlice } from "@reduxjs/toolkit";
//constants

export const initialState = {
    showToast: "",
};

const UtilsSlice = createSlice({
    name: "UtilsSlice",
    initialState,
    reducers: {
        changeToastAction(state: any, action) {
            state.showToast = action.payload;
        },
    },
});

export const { changeToastAction } = UtilsSlice.actions;

export default UtilsSlice.reducer;
