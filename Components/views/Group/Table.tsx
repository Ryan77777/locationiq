import TableContainer from "@common/TableContainer";
import Image from "next/image";
import { useRouter } from "next/router";
import React, { useCallback, useMemo, useState } from "react";
import { Button, Col, Modal, Row } from "react-bootstrap";

// Import Assets
import ILError from "@assets/images/Gifs/1140-error-outline.gif";

const Table: React.FC<any> = ({ data, isVisible, setIsVisible, onDelete }) => {
    const router = useRouter();

    const [selected, setSelected] = useState(null);

    const columns = useMemo(
        () => [
            {
                Header: "No.",
                Cell: ({ row }: any) => row.index + 1,
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "Group Name",
                accessor: "groupName",
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "Actions",
                disableFilters: true,
                filterable: true,
                accessor: (cellProps: any) => {
                    return (
                        <React.Fragment>
                            <Row>
                                <Col>
                                    <Button
                                        type="button"
                                        variant="primary"
                                        className="btn btn-sm"
                                        style={{
                                            marginRight: "4px",
                                        }}
                                        onClick={() =>
                                            redirectToDetail(cellProps)
                                        }
                                    >
                                        <i className="bi bi-eye"></i>
                                    </Button>
                                    <Button
                                        type="button"
                                        variant="warning"
                                        className="btn btn-sm"
                                        style={{
                                            marginRight: "4px",
                                        }}
                                        onClick={() =>
                                            redirectToUpdate(cellProps)
                                        }
                                    >
                                        <i className="bi bi-pencil"></i>
                                    </Button>
                                    <Button
                                        type="button"
                                        variant="danger"
                                        className="btn btn-sm"
                                        onClick={() => {
                                            setIsVisible(true);
                                            setSelected(cellProps.id);
                                        }}
                                    >
                                        <i className="bi bi-trash"></i>
                                    </Button>
                                </Col>
                            </Row>
                        </React.Fragment>
                    );
                },
            },
        ],
        // eslint-disable-next-line react-hooks/exhaustive-deps
        []
    );

    const toggleModal = useCallback(() => {
        setIsVisible(false);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const redirectToCreate = useCallback(
        () => router.push("/app/groups/create"),
        [router]
    );

    const redirectToUpdate = useCallback(
        (data: any) =>
            router.push({
                pathname: "/app/groups/update",
                query: { groupId: data.id },
            }),
        [router]
    );

    const redirectToDetail = useCallback(
        (data: any) =>
            router.push({
                pathname: "/app/groups/detail",
                query: { groupId: data.id },
            }),
        [router]
    );

    return (
        <React.Fragment>
            <TableContainer
                data={data}
                columns={columns}
                isPagination
                isAddGroup
                isGlobalFilter
                isBordered
                customPageSize={10}
                handleUserClick={redirectToCreate}
                className="custom-header-css table align-middle table-nowrap"
                tableClass="table-centered align-middle table-nowrap mb-0"
                theadClass="text-muted table-light"
                SearchPlaceholder="Search..."
            />

            {isVisible && (
                <Modal show={isVisible} onHide={toggleModal} centered>
                    <Modal.Body className="text-center p-5">
                        <Image
                            src={ILError}
                            style={{ width: "120px", height: "120px" }}
                            alt=""
                        />
                        <div className="mt-4">
                            <h3 className="mb-3">Are you sure?</h3>
                            <p className="text-muted mb-4">
                                Please confirm the deletion of this data.
                            </p>
                            <div className="hstack gap-2 justify-content-center">
                                <Button
                                    variant="primary"
                                    onClick={() => onDelete(selected)}
                                >
                                    Delete
                                </Button>
                                <Button variant="danger" onClick={toggleModal}>
                                    Cancel
                                </Button>
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
            )}
        </React.Fragment>
    );
};

export default Table;
