import Layout from "@common/Layout";
import Table from "@component/LogActivities/Table";
import Head from "next/head";
import React, { ReactElement } from "react";
import { Card, Col, Container, Row } from "react-bootstrap";

const LogActivites = () => {
    return (
        <React.Fragment>
            <Head>
                <title>Log Activities | 0320 Xplorer</title>
            </Head>
            <div className="page-content">
                <Container fluid>
                    <Row>
                        <Col lg={12}>
                            <Card>
                                <Card.Header>
                                    <h5 className="card-title mb-0">
                                        Log Activities
                                    </h5>
                                </Card.Header>
                                <Card.Body>
                                    <Table />
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
        </React.Fragment>
    );
};

LogActivites.getLayout = (page: ReactElement) => {
    return <Layout>{page}</Layout>;
};

export default LogActivites;
