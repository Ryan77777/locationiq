import Layout from "@common/Layout";
import Table from "@component/UserManagement/Table";
import { changeToastAction } from "Components/slices/utils/reducer";
import { useDeleteUser, useGetUser } from "Hooks/useUserManagment";
import Head from "next/head";
import React, { ReactElement, useCallback, useEffect, useState } from "react";
import { Card, Col, Container, Row } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";

const UserManagment = () => {
	const dispatch = useDispatch();
	const utils = useSelector((state: any) => state.Utils);

	const { data, refetch } = useGetUser();
	const { mutateAsync } = useDeleteUser();

	const [isLoading, setIsLoading] = useState(false);
	const [isVisible, setIsVisible] = useState(false);

	useEffect(() => {
		if (utils.showToast === "create") {
			toast("Data has been successfully created", {
				position: "top-right",
				hideProgressBar: true,
				className: "bg-success text-white",
			});
		}
		if (utils.showToast === "update") {
			toast("Data has been successfully updated", {
				position: "top-right",
				hideProgressBar: true,
				className: "bg-success text-white",
			});
		}
		if (utils.showToast === "delete") {
			toast("Data has been successfully deleted", {
				position: "top-right",
				hideProgressBar: true,
				className: "bg-success text-white",
			});
		}
		dispatch(changeToastAction(""));
	}, [utils, dispatch]);

	const onDelete = useCallback(async (userId: string) => {
		setIsLoading(true);
		try {
			const response = await mutateAsync(userId);

			if (response) {
				setIsVisible(false);
				setTimeout(() => {
					refetch();
					setIsLoading(false);
					dispatch(changeToastAction("delete"));
				}, 1000);
			}
		} catch (error) {
			console.log(error);
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	return (
		<React.Fragment>
			<Head>
				<title>User Managements | 0320 Xplorer</title>
			</Head>
			<div className="page-content">
				<Container fluid>
					<Row>
						<Col lg={12}>
							<Card>
								<Card.Header>
									<h5 className="card-title mb-0">
										User Managements
									</h5>
								</Card.Header>
								<Card.Body>
									<Table
										data={data ?? []}
										isVisible={isVisible}
										setIsVisible={(value: any) =>
											setIsVisible(value)
										}
										onDelete={(userId: string) =>
											onDelete(userId)
										}
									/>
								</Card.Body>
							</Card>
						</Col>
					</Row>
				</Container>

				{isLoading && (
                    <div
                        style={{
                            background: "rgba(0, 0, 0, 0.5)",
                            position: "absolute",
                            top: 0,
                            bottom: 0,
                            left: 0,
                            right: 0,
                            zIndex: 9999999,
                        }}
                    >
                        <div id="status">
                            <div className="spinner-border text-primary avatar-sm">
                                <span className="visually-hidden">
                                    Loading...
                                </span>
                            </div>
                        </div>
                    </div>
                )}
			</div>
		</React.Fragment>
	);
};

UserManagment.getLayout = (page: ReactElement) => {
	return <Layout>{page}</Layout>;
};

export default UserManagment;
