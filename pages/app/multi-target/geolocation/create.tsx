import Layout from "@common/Layout";
import { changeToastAction } from "Components/slices/utils/reducer";
import { useCreateGeoLocation } from "Hooks/useGeoLocation";
import { useGetGroups } from "Hooks/useGroup";
import dynamic from "next/dynamic";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { ReactElement, useState } from "react";
import { Button, Card, Col, Container, Form, Row } from "react-bootstrap";
import { useDispatch } from "react-redux";

const Select = dynamic(() => import("react-select"), { ssr: false });

const intervalOptions: any = [
    { value: 15, label: "15 minutes" },
    { value: 30, label: "30 minutes" },
    { value: 60, label: "60 minutes" },
];

const CreateGeoLocation = () => {
    const router = useRouter();
    const dispatch = useDispatch();

    const [group, setGroup] = useState<any>({});
    const [interval, setInterval] = useState<number>();

    const [isLoading, setIsLoading] = useState(false);

    const { data } = useGetGroups();
    const { mutateAsync } = useCreateGeoLocation();

    const groupOptions =
        data &&
        data.map((item: any) => ({
            value: item.id,
            label: item.groupName,
        }));

    const handleGroupChange = (value: object) => {
        setGroup(value);
    };

    const handleIntervalChange = (value: number) => {
        setInterval(value);
    };

    const onSubmit = async () => {
        setIsLoading(true);
        try {
            const dataForSubmit = {
                groupId: group.groupId,
                groupName: group.groupName,
                jobInterval: interval,
                status: "CANCELLING",
            };

            const response: any = await mutateAsync(dataForSubmit);

            if (response) {
                setTimeout(() => {
                    setIsLoading(false);
                    dispatch(changeToastAction("create"));
                    router.push("/app/multi-target/geolocation");
                }, 1000);
            }
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <React.Fragment>
            <Head>
                <title>Multi Target | 0320 Xplorer</title>
            </Head>
            <div className="page-content">
                <Container fluid>
                    <Row>
                        <Col lg={12}>
                            <Card>
                                <Card.Header>
                                    <h5 className="card-title mb-0">
                                        Create New GeoLocation
                                    </h5>
                                </Card.Header>
                                <Card.Body>
                                    <Row>
                                        <Col lg={4} className="mb-4">
                                            <Form.Label>
                                                Select Group
                                            </Form.Label>
                                            <Select
                                                name="group"
                                                className="basic-single"
                                                classNamePrefix="select"
                                                defaultValue="Select Group"
                                                options={groupOptions}
                                                onChange={(e: any) =>
                                                    handleGroupChange({
                                                        groupId: e.value,
                                                        groupName: e.label,
                                                    })
                                                }
                                            />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col lg={4} className="mb-4">
                                            <Form.Label>
                                                Select Interval
                                            </Form.Label>
                                            <Select
                                                name="interval"
                                                className="basic-single"
                                                classNamePrefix="select"
                                                defaultValue="Select Interval"
                                                options={intervalOptions}
                                                onChange={(e: any) =>
                                                    handleIntervalChange(
                                                        e.value
                                                    )
                                                }
                                            />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <Button
                                                className="btn-primary mt-3"
                                                onClick={onSubmit}
                                            >
                                                Create GeoLocation
                                            </Button>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Container>

                {isLoading && (
                    <div
                        style={{
                            background: "rgba(0, 0, 0, 0.5)",
                            position: "absolute",
                            top: 0,
                            bottom: 0,
                            left: 0,
                            right: 0,
                            zIndex: 9999999,
                        }}
                    >
                        <div id="status">
                            <div className="spinner-border text-primary avatar-sm">
                                <span className="visually-hidden">
                                    Loading...
                                </span>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        </React.Fragment>
    );
};

CreateGeoLocation.getLayout = (page: ReactElement) => {
    return <Layout>{page}</Layout>;
};

export default CreateGeoLocation;
