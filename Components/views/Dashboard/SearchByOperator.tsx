import { APIClient } from "Components/helpers/api_helper";
import ECharts from "echarts-for-react";
import React, { useEffect, useState } from "react";
import { Card, Col } from "react-bootstrap";

const SearchByOperator = () => {
    const API = new APIClient();

    const [label, setLabel] = useState<any>([]);
    const [value, setValue] = useState<any>([]);
    const series: any = [44, 55, 23, 21];

    const options = {
        polar: {
            radius: [30, "80%"],
        },
        angleAxis: {
            max: 120,
            startAngle: 75,
            axisTick: {
                show: false,
            },
            splitLine: {
                show: false,
            },
            axisLine: {
                lineStyle: {
                    color: "rgba(154, 159, 171, .7)",
                },
            },
        },
        radiusAxis: {
            type: "category",
            data: label,
            axisLabel: {
                show: false,
            },
        },
        tooltip: {},
        series: {
            type: "bar",
            data: value,
            coordinateSystem: "polar",
            label: {
                show: true,
                position: "middle",
                formatter: "{c}",
                color: "#fff",
            },
        },
    };

    const getData = async () => {
        try {
            const obj = JSON.parse(localStorage.getItem("authUser") || "");
            const response: any = await API.get(
                `/stat-by-net-provider/${obj.id}`
            );

            const providerMapping: any = {
                TELKOMSEL: {
                    label: "Telkomsel",
                    color: "#FF5733",
                },
                INDOSAT: {
                    label: "Indosat",
                    color: "#00FF00",
                },
                XL: {
                    label: "XL Axiata",
                    color: "#8A2BE2",
                },
                EXCELCOM: {
                    label: "EXCELCOM",
                    color: "#8A2BE2",
                },
                SMARTFREN: {
                    label: "Smartfren",
                    color: "#FF1493",
                },
                AXIS: {
                    label: "Axis",
                    color: "#8A2BE2",
                },
                THREE: {
                    label: "3 (Three)",
                    color: "#FF4500",
                },
                BOLT: {
                    label: "Bolt",
                    color: "#000080",
                },
                CERIA: {
                    label: "Ceria",
                    color: "#800000",
                },
                HUTCHISON: {
                    label: "Hutchison",
                    color: "#4B0082",
                },
            };

            const filteredData = response.filter(
                (item: any) => item.netProviderName !== null
            );

            const formattedLabel = filteredData.map((item: any) => {
                const providerInfo = providerMapping[item.netProviderName];
                return providerInfo.label;
            });
            const formattedValue = filteredData.map((item: any) => {
                const formattedItem = {
                    value: item.count,
                    itemStyle: {
                        color: providerMapping[item.netProviderName].color,
                    },
                };
                return formattedItem;
            });
            setLabel(formattedLabel);
            setValue(formattedValue);
        } catch (error) {
            console.log(error);
        }
    };

    useEffect(() => {
        getData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <React.Fragment>
            <Col xl={4}>
                <Card className="card-height-100">
                    <Card.Header className="align-items-center d-flex">
                        <h4 className="card-title mb-0 flex-grow-1">
                            Search by Operator
                        </h4>
                        <div className="flex-shrink-0"></div>
                    </Card.Header>

                    <Card.Body>
                        <div id="multiple_radialbar" dir="ltr">
                            <ECharts
                                series={series}
                                option={options}
                                height={328.7}
                            />
                        </div>
                    </Card.Body>
                </Card>
            </Col>
        </React.Fragment>
    );
};

export default SearchByOperator;
