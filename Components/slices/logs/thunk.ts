import axios from "axios";
import { apiError, storeLogs } from "./reducer";

const BASE_URL = process.env.NEXT_PUBLIC_BASE_URL_AUTH;

export const getAllLogs = () => async (dispatch: any) => {
    try {
        const response: any = await axios.get(`${BASE_URL}/logsAll`);

        const data = response.data;

        if (response.status == 200) {
            dispatch(storeLogs(data));
        }
    } catch (error: any) {
        dispatch(apiError("No data founds"));
    }
};

export const getLogsByUser = (id: string) => async (dispatch: any) => {
    try {
        const response: any = await axios.get(`${BASE_URL}/logs/${id}`);

        const data = response.data;

        if (response.status == 200) {
            dispatch(storeLogs(data));
        }
    } catch (error: any) {
        dispatch(apiError("No data founds"));
    }
};
