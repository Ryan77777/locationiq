import ILError from "@assets/images/Gifs/1140-error-outline.gif";
import FotoPlaceholder from "@assets/images/placeholder.jpeg";
import Layout from "@common/Layout";
import { APIClient } from "Components/helpers/api_helper";
import Head from "next/head";
import Image from "next/image";
import { useRouter } from "next/router";
import React, { ReactElement, useCallback, useEffect, useState } from "react";
import {
    Button,
    Card,
    Col,
    Container,
    Modal,
    Row,
    Table,
} from "react-bootstrap";

const API = new APIClient();

function formatTimestamp(timestamp: string) {
    var date = new Date(timestamp);

    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();

    var formattedDate =
        (day < 10 ? "0" : "") +
        day +
        "-" +
        (month < 10 ? "0" : "") +
        month +
        "-" +
        year;

    return formattedDate;
}

const KTPDetail = () => {
    const router = useRouter();
    const nik = router.query.nik;

    const [isLoading, setIsLoading] = useState(false);
    const [isVisible, setIsVisible] = useState(false);

    // New Data
    const [data, setData] = useState<any>(null);
    const [ktp, setKtp] = useState<any>(null);
    const [telco, setTelco] = useState<any>(null);
    const [samsat, setSamsat] = useState<any>(null);

    const getDetail = async () => {
        setIsLoading(true);
        try {
            const response: any = await API.get(`/person-data/${nik}`);

            setIsLoading(false);
            if (response) {
                setData(response);
            }
        } catch (error) {
            setIsLoading(false);
            setData(null);
        }
    };

    useEffect(() => {
        if (nik) {
            getDetail();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [nik]);

    const getDataDukcapil = useCallback(async (id: string) => {
        setIsLoading(true);
        try {
            const response: any = await API.get(`/dukcapil-nik/${id}`);

            if (response) {
                setKtp(response);
            }
            setIsLoading(false);
        } catch (error) {
            setIsLoading(false);
            setIsVisible(true);
            setKtp(null);
        }
    }, []);

    const getDataTelco = useCallback(async (id: string) => {
        setIsLoading(true);
        try {
            const response: any = await API.get(`/telko-nik/${id}`);

            setIsLoading(false);
            if (response) {
                setTelco(response);
            }
        } catch (error) {
            setIsLoading(false);
            setTelco(null);
            setIsVisible(true);
        }
    }, []);

    const getDataSamsat = useCallback(async (id: string) => {
        setIsLoading(true);
        try {
            const response: any = await API.get(`/samsat-nik/${id}`);

            setIsLoading(false);
            if (response) {
                setSamsat(response);
            }
        } catch (error) {
            setIsLoading(false);
            setSamsat(null);
            setIsVisible(true);
        }
    }, []);

    const toggleModal = useCallback(() => {
        setIsVisible(false);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <React.Fragment>
            <Head>
                <title>E-KTP | 0320 Xplorer</title>
            </Head>
            <div className="page-content">
                <Container fluid>
                    <Row>
                        <Col lg={12}>
                            <Card>
                                <Card.Header>
                                    <h5 className="card-title mb-0">
                                        E-KTP Detail
                                    </h5>
                                </Card.Header>
                                <Card.Body>
                                    {data && (
                                        <>
                                            {/* Dukcapil */}
                                            <Row>
                                                <Card.Header>
                                                    <Row className="align-items-center">
                                                        <Col xl={10}>
                                                            <h5 className="card-title ">
                                                                Data Dukcapil
                                                            </h5>
                                                        </Col>
                                                        <Col xl={2}>
                                                            <Button
                                                                variant="primary"
                                                                onClick={() =>
                                                                    getDataDukcapil(
                                                                        data.nik
                                                                    )
                                                                }
                                                            >
                                                                Get Detail
                                                            </Button>
                                                        </Col>
                                                    </Row>
                                                </Card.Header>
                                                <Col xl={3}>
                                                    <Table hover bordered>
                                                        <tbody>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "30%",
                                                                    }}
                                                                >
                                                                    PHOTO
                                                                </th>
                                                                <td
                                                                    style={{
                                                                        width: "30%",
                                                                    }}
                                                                >
                                                                    {ktp !==
                                                                    null ? (
                                                                        ktp?.FOTO ===
                                                                        null ? (
                                                                            <Image
                                                                                src={
                                                                                    FotoPlaceholder
                                                                                }
                                                                                alt="Photo"
                                                                                width={
                                                                                    150
                                                                                }
                                                                                height={
                                                                                    200
                                                                                }
                                                                            />
                                                                        ) : (
                                                                            <Image
                                                                                src={`data:image/png;base64,${ktp?.FOTO}`}
                                                                                alt="Photo"
                                                                                width={
                                                                                    150
                                                                                }
                                                                                height={
                                                                                    200
                                                                                }
                                                                            />
                                                                        )
                                                                    ) : (
                                                                        <Image
                                                                            src={
                                                                                FotoPlaceholder
                                                                            }
                                                                            alt="Photo"
                                                                            width={
                                                                                150
                                                                            }
                                                                            height={
                                                                                200
                                                                            }
                                                                        />
                                                                    )}
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </Table>
                                                </Col>
                                                <Col xl={4}>
                                                    <Table hover bordered>
                                                        <tbody>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "40%",
                                                                    }}
                                                                >
                                                                    NAMA
                                                                </th>
                                                                <td
                                                                    style={{
                                                                        width: "60%",
                                                                    }}
                                                                >
                                                                    {ktp !==
                                                                    null
                                                                        ? ktp.NAMA_LGKP.toUpperCase() ??
                                                                          "-"
                                                                        : data?.kpu_data_by_nik?.nama?.toUpperCase() ??
                                                                          "-"}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "40%",
                                                                    }}
                                                                >
                                                                    SEX
                                                                </th>
                                                                <td
                                                                    style={{
                                                                        width: "60%",
                                                                    }}
                                                                >
                                                                    {ktp !==
                                                                    null
                                                                        ? ktp.JENIS_KLMIN.toUpperCase() ??
                                                                          "-"
                                                                        : data?.kpu_data_by_nik?.jenis_kelamin?.toUpperCase() ??
                                                                          "-"}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "40%",
                                                                    }}
                                                                >
                                                                    PEKERJAAN
                                                                </th>
                                                                <td
                                                                    style={{
                                                                        width: "60%",
                                                                    }}
                                                                >
                                                                    {ktp !==
                                                                    null
                                                                        ? ktp.JENIS_PKRJN?.toUpperCase() ??
                                                                          "-"
                                                                        : "-"}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "40%",
                                                                    }}
                                                                >
                                                                    MATIRAL
                                                                </th>
                                                                <td
                                                                    style={{
                                                                        width: "60%",
                                                                    }}
                                                                >
                                                                    {ktp !==
                                                                    null
                                                                        ? ktp.STAT_KWN?.toUpperCase() ??
                                                                          "-"
                                                                        : data?.kpu_data_by_nik?.kawin?.toUpperCase() ??
                                                                          "-"}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "40%",
                                                                    }}
                                                                >
                                                                    AGAMA
                                                                </th>
                                                                <td
                                                                    style={{
                                                                        width: "60%",
                                                                    }}
                                                                >
                                                                    {ktp !==
                                                                    null
                                                                        ? ktp.AGAMA?.toUpperCase() ??
                                                                          "-"
                                                                        : "-"}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "40%",
                                                                    }}
                                                                >
                                                                    AYAH
                                                                </th>
                                                                <td
                                                                    style={{
                                                                        width: "60%",
                                                                    }}
                                                                >
                                                                    {ktp !==
                                                                    null
                                                                        ? ktp.NAMA_LGKP_AYAH?.toUpperCase() ??
                                                                          "-"
                                                                        : "-"}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "40%",
                                                                    }}
                                                                >
                                                                    IBU
                                                                </th>
                                                                <td
                                                                    style={{
                                                                        width: "60%",
                                                                    }}
                                                                >
                                                                    {ktp !==
                                                                    null
                                                                        ? ktp.NAMA_LGKP_IBU?.toUpperCase() ??
                                                                          "-"
                                                                        : "-"}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "40%",
                                                                    }}
                                                                >
                                                                    PROVINSI
                                                                </th>
                                                                <td
                                                                    style={{
                                                                        width: "60%",
                                                                    }}
                                                                >
                                                                    {data?.kpu_data_by_nik?.pro?.toUpperCase() ===
                                                                    "TIDAK TAHU"
                                                                        ? "-"
                                                                        : data?.kpu_data_by_nik?.pro?.toUpperCase()}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "40%",
                                                                    }}
                                                                >
                                                                    KABUPATEN
                                                                </th>
                                                                <td
                                                                    style={{
                                                                        width: "60%",
                                                                    }}
                                                                >
                                                                    {data?.kpu_data_by_nik?.kab?.toUpperCase() ===
                                                                    "TIDAK TAHU"
                                                                        ? "-"
                                                                        : data?.kpu_data_by_nik?.kab?.toUpperCase()}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "40%",
                                                                    }}
                                                                >
                                                                    KECAMATAN
                                                                </th>
                                                                <td
                                                                    style={{
                                                                        width: "60%",
                                                                    }}
                                                                >
                                                                    {data?.kpu_data_by_nik?.kec?.toUpperCase() ===
                                                                    "TIDAK TAHU"
                                                                        ? "-"
                                                                        : data?.kpu_data_by_nik?.kec?.toUpperCase()}
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </Table>
                                                </Col>
                                                <Col xl={4}>
                                                    <Table hover bordered>
                                                        <tbody>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "40%",
                                                                    }}
                                                                >
                                                                    KELURAHAN
                                                                </th>
                                                                <td
                                                                    style={{
                                                                        width: "60%",
                                                                    }}
                                                                >
                                                                    {data?.kpu_data_by_nik?.kel?.toUpperCase() ===
                                                                    "TIDAK TAHU"
                                                                        ? "-"
                                                                        : data?.kpu_data_by_nik?.kel?.toUpperCase()}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "40%",
                                                                    }}
                                                                >
                                                                    ALAMAT
                                                                </th>
                                                                <td
                                                                    style={{
                                                                        width: "60%",
                                                                    }}
                                                                >
                                                                    {ktp !==
                                                                    null
                                                                        ? ktp.ALAMAT.toUpperCase() ??
                                                                          "-"
                                                                        : data?.kpu_data_by_nik?.alamat?.toUpperCase() ===
                                                                          "TIDAK TAHU"
                                                                        ? "-"
                                                                        : data?.kpu_data_by_nik?.alamat?.toUpperCase()}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "40%",
                                                                    }}
                                                                >
                                                                    RW
                                                                </th>
                                                                <td
                                                                    style={{
                                                                        width: "60%",
                                                                    }}
                                                                >
                                                                    {data?.kpu_data_by_nik?.rw?.toUpperCase() ===
                                                                    "TIDAK TAHU"
                                                                        ? "-"
                                                                        : data?.kpu_data_by_nik?.rw?.toUpperCase()}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "40%",
                                                                    }}
                                                                >
                                                                    RT
                                                                </th>
                                                                <td
                                                                    style={{
                                                                        width: "60%",
                                                                    }}
                                                                >
                                                                    {data?.kpu_data_by_nik?.rt?.toUpperCase() ===
                                                                    "TIDAK TAHU"
                                                                        ? "-"
                                                                        : data?.kpu_data_by_nik?.rt?.toUpperCase()}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "40%",
                                                                    }}
                                                                >
                                                                    GOL DARAH
                                                                </th>
                                                                <td
                                                                    style={{
                                                                        width: "60%",
                                                                    }}
                                                                >
                                                                    {ktp !==
                                                                    null
                                                                        ? ktp?.GOL_DARAH?.toUpperCase() ??
                                                                          "-"
                                                                        : "-"}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "40%",
                                                                    }}
                                                                >
                                                                    NIK
                                                                </th>
                                                                <td
                                                                    style={{
                                                                        width: "60%",
                                                                    }}
                                                                >
                                                                    {data?.nik ??
                                                                        "-"}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "40%",
                                                                    }}
                                                                >
                                                                    NKK
                                                                </th>
                                                                <td
                                                                    style={{
                                                                        width: "60%",
                                                                    }}
                                                                >
                                                                    {data?.nkk ??
                                                                        "-"}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "40%",
                                                                    }}
                                                                >
                                                                    TEMPAT LAHIR
                                                                </th>
                                                                <td
                                                                    style={{
                                                                        width: "60%",
                                                                    }}
                                                                >
                                                                    {ktp !==
                                                                    null
                                                                        ? ktp.TMPT_LHR.toUpperCase() ??
                                                                          "-"
                                                                        : data?.kpu_data_by_nik?.tempat_lahir?.toUpperCase() ===
                                                                          "TIDAK TAHU"
                                                                        ? "-"
                                                                        : data?.kpu_data_by_nik?.tempat_lahir?.toUpperCase()}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "40%",
                                                                    }}
                                                                >
                                                                    TANGGAL
                                                                    LAHIR
                                                                </th>
                                                                <td
                                                                    style={{
                                                                        width: "60%",
                                                                    }}
                                                                >
                                                                    {data?.dob ??
                                                                        "-"}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "40%",
                                                                    }}
                                                                >
                                                                    CREATED
                                                                </th>
                                                                <td
                                                                    style={{
                                                                        width: "60%",
                                                                    }}
                                                                >
                                                                    {formatTimestamp(
                                                                        data?.created_at
                                                                    ) ?? "-"}
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </Table>
                                                </Col>
                                            </Row>

                                            {/* Alamat Lengkap */}
                                            <Row>
                                                <Col xl={12}>
                                                    <Table hover bordered>
                                                        <tbody>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "20%",
                                                                    }}
                                                                >
                                                                    ALAMAT
                                                                    LENGKAP
                                                                </th>
                                                                <td
                                                                    style={{
                                                                        width: "80%",
                                                                    }}
                                                                >
                                                                    {ktp !==
                                                                    null
                                                                        ? ktp.ALAMAT.toUpperCase()
                                                                        : data?.kpu_data_by_nik?.alamat.toUpperCase() ===
                                                                          "TIDAK TAHU"
                                                                        ? "-"
                                                                        : data?.kpu_data_by_nik?.alamat.toUpperCase()}
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </Table>
                                                </Col>
                                            </Row>

                                            {/* Weton */}
                                            <Row>
                                                <Card.Header>
                                                    <h5 className="card-title ">
                                                        Karakteristik
                                                        Berdasarkan Tanggal
                                                        Lahir
                                                    </h5>
                                                </Card.Header>
                                                <Col xl={12}>
                                                    <Table hover bordered>
                                                        <tbody>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "20%",
                                                                    }}
                                                                >
                                                                    Weton
                                                                </th>
                                                                <td
                                                                    style={{
                                                                        width: "80%",
                                                                    }}
                                                                >
                                                                    {data
                                                                        ?.karakter_data_by_dob
                                                                        ?.Weton
                                                                        ?.weton ??
                                                                        "-"}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "20%",
                                                                    }}
                                                                >
                                                                    Ramalan
                                                                    Weton
                                                                </th>
                                                                <td
                                                                    style={{
                                                                        width: "80%",
                                                                    }}
                                                                >
                                                                    {data
                                                                        ?.karakter_data_by_dob
                                                                        ?.Weton
                                                                        ?.karakter_weton ??
                                                                        "-"}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "20%",
                                                                    }}
                                                                >
                                                                    Zodiak
                                                                </th>
                                                                <td
                                                                    style={{
                                                                        width: "80%",
                                                                    }}
                                                                >
                                                                    {data
                                                                        ?.karakter_data_by_dob
                                                                        ?.Zodiak
                                                                        ?.zodiak ??
                                                                        "-"}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "20%",
                                                                    }}
                                                                >
                                                                    Ramalan
                                                                    Zodiak
                                                                </th>
                                                                <td
                                                                    style={{
                                                                        width: "80%",
                                                                    }}
                                                                >
                                                                    <li>
                                                                        <b className="text-white">
                                                                            Sifat:{" "}
                                                                        </b>
                                                                        {data
                                                                            ?.karakter_data_by_dob
                                                                            ?.Zodiak
                                                                            ?.profil ??
                                                                            "-"}
                                                                    </li>
                                                                    <li>
                                                                        <b className="text-white">
                                                                            Favorit:{" "}
                                                                        </b>
                                                                        {data
                                                                            ?.karakter_data_by_dob
                                                                            ?.Zodiak
                                                                            ?.favorit ??
                                                                            "-"}
                                                                    </li>
                                                                    <li>
                                                                        <b className="text-white">
                                                                            Hobi:{" "}
                                                                        </b>
                                                                        {data
                                                                            ?.karakter_data_by_dob
                                                                            ?.Zodiak
                                                                            ?.hobi ??
                                                                            "-"}
                                                                    </li>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "20%",
                                                                    }}
                                                                >
                                                                    Shio
                                                                </th>
                                                                <td
                                                                    style={{
                                                                        width: "80%",
                                                                    }}
                                                                >
                                                                    {data
                                                                        ?.karakter_data_by_dob
                                                                        ?.Shio
                                                                        ?.shio ??
                                                                        "-"}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "20%",
                                                                    }}
                                                                >
                                                                    Ramalan Shio
                                                                </th>
                                                                <td
                                                                    style={{
                                                                        width: "80%",
                                                                    }}
                                                                >
                                                                    {data
                                                                        ?.karakter_data_by_dob
                                                                        ?.Shio
                                                                        ?.karakter_shio ??
                                                                        "-"}
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </Table>
                                                </Col>
                                            </Row>

                                            {/* Telco */}
                                            <Row>
                                                <Card.Header>
                                                    <Row className="align-items-center">
                                                        <Col xl={10}>
                                                            <h5 className="card-title ">
                                                                Daftar Nomor
                                                                Telepon
                                                            </h5>
                                                        </Col>
                                                        <Col xl={2}>
                                                            <Button
                                                                variant="primary"
                                                                disabled={
                                                                    telco
                                                                        ? true
                                                                        : false
                                                                }
                                                                onClick={() =>
                                                                    getDataTelco(
                                                                        data.nik
                                                                    )
                                                                }
                                                            >
                                                                Get Detail
                                                            </Button>
                                                        </Col>
                                                    </Row>
                                                </Card.Header>
                                                <Col xl={12}>
                                                    <Table hover bordered>
                                                        <thead>
                                                            <tr>
                                                                <th
                                                                    style={{
                                                                        width: "25%",
                                                                    }}
                                                                >
                                                                    Nomor
                                                                    Telepon
                                                                </th>
                                                                <th
                                                                    style={{
                                                                        width: "25%",
                                                                    }}
                                                                >
                                                                    Provider
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {telco ? (
                                                                telco?.map(
                                                                    (
                                                                        item: any,
                                                                        index: number
                                                                    ) => (
                                                                        <tr
                                                                            key={
                                                                                index
                                                                            }
                                                                        >
                                                                            <td
                                                                                style={{
                                                                                    width: "25%",
                                                                                }}
                                                                            >
                                                                                {
                                                                                    item.NO_PESERTA
                                                                                }
                                                                            </td>
                                                                            <td
                                                                                style={{
                                                                                    width: "25%",
                                                                                }}
                                                                            >
                                                                                {
                                                                                    item.INSTANSI
                                                                                }
                                                                            </td>
                                                                        </tr>
                                                                    )
                                                                )
                                                            ) : (
                                                                <tr>
                                                                    <td
                                                                        colSpan={
                                                                            2
                                                                        }
                                                                        style={{
                                                                            width: "100%",
                                                                            textAlign:
                                                                                "center",
                                                                        }}
                                                                    >
                                                                        No Data
                                                                        Found
                                                                    </td>
                                                                </tr>
                                                            )}
                                                        </tbody>
                                                    </Table>
                                                </Col>
                                            </Row>

                                            {/* Samsat */}
                                            <Row>
                                                <Card.Header>
                                                    <Row className="align-items-center">
                                                        <Col xl={10}>
                                                            <h5 className="card-title ">
                                                                Data Kendaraan
                                                            </h5>
                                                        </Col>
                                                        <Col xl={2}>
                                                            <Button
                                                                variant="primary"
                                                                disabled={
                                                                    samsat
                                                                        ? true
                                                                        : false
                                                                }
                                                                onClick={() =>
                                                                    getDataSamsat(
                                                                        data.nik
                                                                    )
                                                                }
                                                            >
                                                                Get Detail
                                                            </Button>
                                                        </Col>
                                                    </Row>
                                                </Card.Header>
                                                {samsat ? (
                                                    <Col xl={12}>
                                                        <Table hover bordered>
                                                            <thead>
                                                                <tr>
                                                                    <th>
                                                                        PEMILIK
                                                                    </th>
                                                                    <th>
                                                                        NO
                                                                        POLISI
                                                                    </th>
                                                                    <th>
                                                                        MERK
                                                                    </th>
                                                                    <th>
                                                                        NO BPKB
                                                                    </th>
                                                                    <th>
                                                                        NO
                                                                        RANGKA
                                                                    </th>
                                                                    <th>
                                                                        NO MESIN
                                                                    </th>
                                                                    <th>
                                                                        TIPE
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                {samsat ? (
                                                                    samsat?.map(
                                                                        (
                                                                            item: any,
                                                                            index: number
                                                                        ) => (
                                                                            <tr
                                                                                key={
                                                                                    index
                                                                                }
                                                                            >
                                                                                <td>
                                                                                    {
                                                                                        item.PEMILIK
                                                                                    }
                                                                                </td>
                                                                                <td>
                                                                                    {
                                                                                        item.NO_POLISI
                                                                                    }
                                                                                </td>
                                                                                <td>
                                                                                    {
                                                                                        item.MERK
                                                                                    }
                                                                                </td>
                                                                                <td>
                                                                                    {
                                                                                        item.NO_BPKB
                                                                                    }
                                                                                </td>
                                                                                <td>
                                                                                    {
                                                                                        item.NO_RANGKA
                                                                                    }
                                                                                </td>
                                                                                <td>
                                                                                    {
                                                                                        item.NO_MESIN
                                                                                    }
                                                                                </td>
                                                                                <td>
                                                                                    {
                                                                                        item.TIPE_KEND
                                                                                    }
                                                                                </td>
                                                                            </tr>
                                                                        )
                                                                    )
                                                                ) : (
                                                                    <tr>
                                                                        <td
                                                                            colSpan={
                                                                                2
                                                                            }
                                                                            style={{
                                                                                width: "100%",
                                                                                textAlign:
                                                                                    "center",
                                                                            }}
                                                                        >
                                                                            No
                                                                            Data
                                                                            Found
                                                                        </td>
                                                                    </tr>
                                                                )}
                                                            </tbody>
                                                        </Table>
                                                    </Col>
                                                ) : (
                                                    <Col xl={12}>
                                                        <Table hover bordered>
                                                            <tbody>
                                                                <tr>
                                                                    <td
                                                                        style={{
                                                                            width: "100%",
                                                                            textAlign:
                                                                                "center",
                                                                        }}
                                                                    >
                                                                        No Data
                                                                        Found
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </Table>
                                                    </Col>
                                                )}
                                            </Row>
                                        </>
                                    )}
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>

            {isLoading && (
                <div
                    style={{
                        background: "rgba(0, 0, 0, 0.5)",
                        position: "fixed",
                        top: 0,
                        bottom: 0,
                        left: 0,
                        right: 0,
                        zIndex: 9999999,
                    }}
                >
                    <div id="status">
                        <div className="spinner-border text-primary avatar-sm">
                            <span className="visually-hidden">Loading...</span>
                        </div>
                    </div>
                </div>
            )}

            <Modal show={isVisible} onHide={toggleModal} centered>
                <Modal.Body className="text-center p-5">
                    <Image
                        src={ILError}
                        style={{ width: "120px", height: "120px" }}
                        alt=""
                    />
                    <div className="mt-4">
                        <h3 className="mb-3">Oops, data not found!</h3>
                        <p className="text-muted mb-4">
                            The data was not successfully found.
                        </p>
                    </div>
                </Modal.Body>
            </Modal>
        </React.Fragment>
    );
};
KTPDetail.getLayout = (page: ReactElement) => {
    return <Layout>{page}</Layout>;
};

export default KTPDetail;
