import { APIClient } from "Components/helpers/api_helper";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import { Card, Col } from "react-bootstrap";

function formatDateTime(timestamp: any) {
    const date = new Date(timestamp * 1000);
    const formattedDate = date.toISOString().slice(0, 19).replace("T", " ");
    return formattedDate;
}

const SearchHistory = () => {
    const API = new APIClient();

    const [data, setData] = useState<any>([]);

    const getData = async () => {
        try {
            const obj = JSON.parse(localStorage.getItem("authUser") || "");
            const response: any = await API.get(
                `/get-search-history/${obj.id}`
            );

            setData(response);
        } catch (error) {
            console.log(error);
        }
    };

    useEffect(() => {
        getData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const lastFiveData = data.slice(-5);
    return (
        <React.Fragment>
            <Col xl={8}>
                <Card>
                    <Card.Header className="align-items-center d-flex">
                        <h4 className="card-title mb-0 flex-grow-1">
                            5 Recent searches
                        </h4>
                    </Card.Header>

                    <Card.Body>
                        <div className="table-responsive table-card">
                            <table className="table table-borderless table-centered table-hover align-middle table-nowrap mb-0">
                                <thead className="table-light">
                                    <tr>
                                        <th scope="col">No.</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Phone number</th>
                                        <th scope="col">Updated Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {lastFiveData.map(
                                        (item: any, key: number) => (
                                            <tr key={key}>
                                                <td>
                                                    <Link
                                                        href="#!"
                                                        className="fw-medium link-primary"
                                                    >
                                                        {key + 1}
                                                    </Link>
                                                </td>
                                                <td>
                                                    <div className="d-flex align-items-center">
                                                        <div className="flex-grow-1">
                                                            {item.username}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>{item.phone}</td>
                                                <td>
                                                    {formatDateTime(
                                                        item.createdAt
                                                    )}
                                                </td>
                                            </tr>
                                        )
                                    )}
                                </tbody>
                            </table>
                        </div>
                    </Card.Body>
                </Card>
            </Col>
        </React.Fragment>
    );
};

export default SearchHistory;
