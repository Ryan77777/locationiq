const historyData = [
    {
        id: 1,
        name: "Samsul",
        date: "16 Aug, 2022",
        phone: "081234580987",
    },
    {
        id: 2,
        name: "Haidar",
        date: "05 Sep, 2022",
        phone: "081234580982",
    },
    {
        id: 3,
        name: "Kiki",
        date: "28 Sep, 2022",
        phone: "081334580982",
    },
    {
        id: 4,
        name: "Nasrudin",
        date: "02 Oct, 2022",
        phone: "083234580982",
    },
    {
        id: 5,
        name: "Ikbal",
        date: "11 Oct, 2022",
        phone: "091234580982",
    },
    {
        id: 6,
        name: "Jeje",
        date: "19 Nov, 2022",
        phone: "0812345230982",
    },
];

export { historyData };
