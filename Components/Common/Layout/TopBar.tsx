import React, { useEffect, useRef, useState } from "react";
import { Col, Row } from "react-bootstrap";

const TopBar = () => {
    const currentTime: any = useRef(null);

    useEffect(() => {
        const interval = setInterval(() => {
            // date
            var d = new Date();
            var dateOptions: object = {
                weekday: "short",
                month: "short",
                day: "numeric",
            };
            var date = d.toLocaleDateString(undefined, dateOptions);
            // time
            var hours = d.getHours();
            var ampm = hours >= 12 ? " PM" : " AM";
            var hours = hours % 12;
            var time =
                ("0" + hours).slice(-2) +
                ":" +
                ("0" + d.getMinutes()).slice(-2) +
                ampm;
            currentTime.current.innerHTML = date + " | " + time;
        }, 1000);
        return () => clearInterval(interval);
    }, [currentTime]);

    return (
        <React.Fragment>
            <div className="top-tagbar">
                <div className="w-100">
                    <Row className="justify-content-between align-items-center">
                        <Col className="col-md-auto" xs={9}>
                            <div className="text-white-50 fs-13">
                                <i className="bi bi-clock align-middle me-2"></i>{" "}
                                <span
                                    ref={currentTime}
                                    id="current-time"
                                ></span>
                            </div>
                        </Col>
                        <Col
                            xs={6}
                            className="d-none d-lg-block col-md-auto d-none d-lg-block"
                        >
                            <div className="d-flex align-items-center justify-content-center gap-3 fs-13 text-white-50">
                                <div>
                                    <i className="bi bi-envelope align-middle me-2"></i>{" "}
                                    support@locationiq.com
                                </div>
                                <div>
                                    <i className="bi bi-globe align-middle me-2"></i>{" "}
                                    www.locationiq.com
                                </div>
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        </React.Fragment>
    );
};

export default TopBar;
