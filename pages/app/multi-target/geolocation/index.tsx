import Layout from "@common/Layout";
import Table from "@component/GeoLocation/Table";
import { changeToastAction } from "Components/slices/utils/reducer";
import {
    useDeleteGeoLocation,
    useGetGeoLocation,
    useUpdateGeoLocation,
} from "Hooks/useGeoLocation";
import Head from "next/head";
import React, { ReactElement, useCallback, useEffect, useState } from "react";
import { Card, Col, Container, Row } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";

const GeoLocation = () => {
    const dispatch = useDispatch();
    const utils = useSelector((state: any) => state.Utils);

    const { data, refetch } = useGetGeoLocation();
    const { mutateAsync } = useDeleteGeoLocation();
    const { mutateAsync: updateAsync } = useUpdateGeoLocation();

    const [isLoading, setIsLoading] = useState(false);
    const [isVisible, setIsVisible] = useState(false);

    useEffect(() => {
        if (utils.showToast === "run") {
            toast("Task has been executed", {
                position: "top-right",
                hideProgressBar: true,
                className: "bg-success text-white",
            });
        }
        if (utils.showToast === "stop") {
            toast("Task has been stopped", {
                position: "top-right",
                hideProgressBar: true,
                className: "bg-success text-white",
            });
        }
        if (utils.showToast === "create") {
            toast("Data has been successfully created", {
                position: "top-right",
                hideProgressBar: true,
                className: "bg-success text-white",
            });
        }
        if (utils.showToast === "update") {
            toast("Data has been successfully updated", {
                position: "top-right",
                hideProgressBar: true,
                className: "bg-success text-white",
            });
        }
        if (utils.showToast === "delete") {
            toast("Data has been successfully deleted", {
                position: "top-right",
                hideProgressBar: true,
                className: "bg-success text-white",
            });
        }
        dispatch(changeToastAction(""));
    }, [utils, dispatch]);

    const onDelete = useCallback(async (id: string) => {
        setIsLoading(true);
        try {
            const response = await mutateAsync(id);

            if (response) {
                setIsVisible(false);
                setTimeout(() => {
                    refetch();
                    setIsLoading(false);
                    dispatch(changeToastAction("delete"));
                }, 1000);
            }
        } catch (error) {
            console.log(error);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const runningAction = useCallback(
        async (data: any) => {
            setIsLoading(true);
            try {
                const dataForSubmit = {
                    id: data.id,
                    groupId: data.groupId,
                    groupName: data.groupName,
                    jobInterval: data.jobInterval,
                    status: "STARTING",
                };

                const response: any = await updateAsync(dataForSubmit);

                if (response) {
                    setTimeout(() => {
                        refetch();
                        setIsLoading(false);
                        dispatch(changeToastAction("run"));
                    }, 1000);
                }
            } catch (error) {
                console.log(error);
            }
        },
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [dispatch]
    );

    const stopAction = useCallback(
        async (data: any) => {
            setIsLoading(true);
            try {
                const dataForSubmit = {
                    id: data.id,
                    groupId: data.groupId,
                    groupName: data.groupName,
                    jobInterval: data.jobInterval,
                    status: "CANCELLING",
                };

                const response: any = await updateAsync(dataForSubmit);

                if (response) {
                    setTimeout(() => {
                        refetch();
                        setIsLoading(false);
                        dispatch(changeToastAction("stop"));
                    }, 1000);
                }
            } catch (error) {
                console.log(error);
            }
        },
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [dispatch]
    );

    return (
        <React.Fragment>
            <Head>
                <title>Multi Target | 0320 Xplorer</title>
            </Head>
            <div className="page-content">
                <Container fluid>
                    <Row>
                        <Col lg={12}>
                            <Card>
                                <Card.Header>
                                    <h5 className="card-title mb-0">
                                        GeoLocation
                                    </h5>
                                </Card.Header>
                                <Card.Body>
                                    <Table
                                        data={data ?? []}
                                        isVisible={isVisible}
                                        setIsVisible={setIsVisible}
                                        onDelete={(groupId: string) =>
                                            onDelete(groupId)
                                        }
                                        runningAction={(data: any) =>
                                            runningAction(data)
                                        }
                                        stopAction={(data: any) =>
                                            stopAction(data)
                                        }
                                    />
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Container>

                {isLoading && (
                    <div
                        style={{
                            background: "rgba(0, 0, 0, 0.5)",
                            position: "absolute",
                            top: 0,
                            bottom: 0,
                            left: 0,
                            right: 0,
                            zIndex: 9999999,
                        }}
                    >
                        <div id="status">
                            <div className="spinner-border text-primary avatar-sm">
                                <span className="visually-hidden">
                                    Loading...
                                </span>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        </React.Fragment>
    );
};

GeoLocation.getLayout = (page: ReactElement) => {
    return <Layout>{page}</Layout>;
};

export default GeoLocation;
