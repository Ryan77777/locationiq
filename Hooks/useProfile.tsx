import { getLoggedinUser } from "Components/helpers/api_helper";
import { useEffect, useState } from "react";

const useProfile = () => {
    const userProfileSession = getLoggedinUser();
    const [loading, setLoading] = useState(true);
    const [userProfile, setUserProfile] = useState(
        userProfileSession ? userProfileSession : null
    );

    useEffect(() => {
        const userProfileSession = getLoggedinUser();
        setUserProfile(userProfileSession ? userProfileSession : null);
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    return { userProfile, loading };
};

export { useProfile };
