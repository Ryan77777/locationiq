import { UseQueryResult, useQuery } from "@tanstack/react-query";
import { APIClient } from "Components/helpers/api_helper";

const BASE_URL = process.env.NEXT_PUBLIC_BASE_URL;

const API = new APIClient();

export const useGetLogs = (): UseQueryResult<any, Error> => {
    return useQuery<any, Error>({
        queryKey: ["logs"],
        queryFn: async () => {
            const obj = JSON.parse(localStorage.getItem("authUser") || "");
            const response: any = await API.get(`/logs/${obj.id}`);
            return response;
        },
        retry: true,
        refetchOnWindowFocus: true,
        refetchOnMount: true,
    });
};
