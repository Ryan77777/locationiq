import React, { Fragment } from "react";
import PropTypes from "prop-types";
import {
    useTable,
    useGlobalFilter,
    useSortBy,
    useFilters,
    useExpanded,
    usePagination,
} from "react-table";
import { Table, Row, Col, Button, Spinner } from "react-bootstrap";
import { Filter, DefaultColumnFilter } from "./Filter";

interface GlobalFilterProps {
    preGlobalFilteredRows?: any;
    globalFilter?: any;
    setGlobalFilter?: any;
    SearchPlaceholder?: string;
    isProductsFilter?: boolean;
}

// Define a default UI for filtering
function GlobalFilter({
    preGlobalFilteredRows,
    globalFilter,
    setGlobalFilter,
    SearchPlaceholder,
    isProductsFilter,
}: GlobalFilterProps) {
    const [value, setValue] = React.useState(globalFilter);
    const onChange = (value: any) => {
        setGlobalFilter(value || undefined);
    };

    return (
        <React.Fragment>
            <Col className="col-sm">
                <div className="d-flex justify-content-sm-end">
                    <label htmlFor="search-bar-0" className="search-label">
                        <input
                            onChange={(e) => {
                                setValue(e.target.value);
                                onChange(e.target.value);
                            }}
                            id="search-bar-0"
                            type="text"
                            className="form-control"
                            placeholder={SearchPlaceholder}
                            value={value || ""}
                        />
                    </label>
                </div>
            </Col>
        </React.Fragment>
    );
}

interface TableContainerProps {
    columns?: any;
    data?: any;
    isGlobalFilter?: any;
    isPagination?: any;
    divClassName?: any;
    tableClass?: any;
    theadClass?: any;
    isBordered?: boolean;
    isAddGroup?: any;
    isAddGeoLocation?: any;
    isAddGeoFencing?: any;
    isAddUser?: any;
    isLoading?: boolean;
    handleOrderClicks?: any;
    handleUserClick?: any;
    handleCustomerClick?: any;
    customPageSize?: any;
    className?: any;
    SearchPlaceholder: string;
    isProductsFilter?: boolean;
}

const TableContainer = ({
    columns,
    data,
    tableClass,
    theadClass,
    isBordered,
    isGlobalFilter,
    isPagination,
    isProductsFilter,
    isAddGroup,
    isAddGeoLocation,
    isAddGeoFencing,
    isAddUser,
    isLoading,
    handleUserClick,
    customPageSize,
    SearchPlaceholder,
}: TableContainerProps) => {
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        page,
        prepareRow,
        canPreviousPage,
        canNextPage,
        pageOptions,
        gotoPage,
        nextPage,
        previousPage,
        state,
        preGlobalFilteredRows,
        setGlobalFilter,
        state: { pageIndex },
    } = useTable(
        {
            columns,
            data,
            defaultColumn: { Filter: DefaultColumnFilter },
            initialState: {
                pageIndex: 0,
                pageSize: customPageSize,
            },
        },
        useGlobalFilter,
        useFilters,
        useSortBy,
        useExpanded,
        usePagination
    );

    const generateSortingIndicator = (column: any) => {
        return column.isSorted ? (
            column.isSortedDesc ? (
                <span>&#9650;</span>
            ) : (
                <span>&#9660;</span>
            )
        ) : (
            ""
        );
    };

    return (
        <Fragment>
            <Row className="mb-2">
                {isAddGroup && (
                    <Col sm="7" className="d-flex justify-content-start">
                        <div className="text-sm-end">
                            <Button
                                type="button"
                                variant="primary"
                                className="btn mb-2 me-2"
                                onClick={handleUserClick}
                            >
                                <i className="mdi mdi-plus-circle-outline me-1" />
                                Create New Group
                            </Button>
                        </div>
                    </Col>
                )}
                {isAddGeoLocation && (
                    <Col sm="7" className="d-flex justify-content-start">
                        <div className="text-sm-end">
                            <Button
                                type="button"
                                variant="primary"
                                className="btn mb-2 me-2"
                                onClick={handleUserClick}
                            >
                                <i className="mdi mdi-plus-circle-outline me-1" />
                                Create New GeoLocation
                            </Button>
                        </div>
                    </Col>
                )}
                {isAddGeoFencing && (
                    <Col sm="7" className="d-flex justify-content-start">
                        <div className="text-sm-end">
                            <Button
                                type="button"
                                variant="primary"
                                className="btn mb-2 me-2"
                                onClick={handleUserClick}
                            >
                                <i className="mdi mdi-plus-circle-outline me-1" />
                                Create New GeoFencing
                            </Button>
                        </div>
                    </Col>
                )}
                {isAddUser && (
                    <Col sm="7" className="d-flex justify-content-start">
                        <div className="text-sm-end">
                            <Button
                                type="button"
                                variant="primary"
                                className="btn mb-2 me-2"
                                onClick={handleUserClick}
                            >
                                <i className="mdi mdi-plus-circle-outline me-1" />
                                Create New User
                            </Button>
                        </div>
                    </Col>
                )}
                {isGlobalFilter && (
                    <GlobalFilter
                        preGlobalFilteredRows={preGlobalFilteredRows}
                        globalFilter={state.globalFilter}
                        setGlobalFilter={setGlobalFilter}
                        SearchPlaceholder={SearchPlaceholder}
                        isProductsFilter={isProductsFilter}
                    />
                )}
            </Row>

            <div
                className="table-responsive react-table"
                style={{ overflowX: "auto" }}
            >
                <Table
                    hover
                    {...getTableProps()}
                    className={tableClass}
                    bordered={isBordered}
                >
                    <thead className={theadClass}>
                        {headerGroups.map((headerGroup: any, index: number) => (
                            <Fragment key={index}>
                                <tr {...headerGroup.getHeaderGroupProps()}>
                                    {headerGroup.headers.map(
                                        (column: any, index: number) => (
                                            <th key={index}>
                                                <div
                                                    className="mb-2"
                                                    {...column.getSortByToggleProps()}
                                                >
                                                    {column.render("Header")}
                                                    {generateSortingIndicator(
                                                        column
                                                    )}
                                                </div>
                                                <Filter column={column} />
                                            </th>
                                        )
                                    )}
                                </tr>
                            </Fragment>
                        ))}
                    </thead>

                    <tbody {...getTableBodyProps()}>
                        {page.map((row: any, index: number) => {
                            prepareRow(row);
                            return (
                                <Fragment key={index}>
                                    <tr>
                                        {row.cells.map(
                                            (cell: any, index: number) => {
                                                return (
                                                    <td
                                                        key={index}
                                                        {...cell.getCellProps()}
                                                    >
                                                        {cell.render("Cell")}
                                                    </td>
                                                );
                                            }
                                        )}
                                    </tr>
                                </Fragment>
                            );
                        })}
                        {!isLoading && page.length === 0 && (
                            <tr>
                                <td
                                    colSpan={columns.length}
                                    className="text-center"
                                >
                                    No Data Found
                                </td>
                            </tr>
                        )}
                        {isLoading && (
                            <tr>
                                <td
                                    colSpan={columns.length}
                                    className="text-center"
                                >
                                    <div className="text-center">
                                        <Spinner
                                            animation="border"
                                            variant="primary"
                                        />
                                    </div>
                                </td>
                            </tr>
                        )}
                    </tbody>
                </Table>
            </div>
            {isPagination && (
                <Row className="align-items-center mt-2 py-2 px-2 gy-2 text-center text-sm-start">
                    <div className="col-sm">
                        <div className="text-muted">
                            Showing{" "}
                            <span className="fw-semibold">{pageIndex + 1}</span>{" "}
                            of{" "}
                            <span className="fw-semibold">
                                {pageOptions.length}
                            </span>{" "}
                            Results
                        </div>
                    </div>
                    <div className="col-sm-auto">
                        <ul className="pagination pagination-separated mb-0 justify-content-center justify-content-sm-start">
                            <li
                                className={
                                    !canPreviousPage
                                        ? "page-item disabled"
                                        : "page-item"
                                }
                                onClick={previousPage}
                            >
                                <Button variant="link" className="page-link">
                                    Previous
                                </Button>
                            </li>
                            {pageOptions.length > 1 && (
                                <ul className="pagination">
                                    {pageOptions.map(
                                        (item: any, key: number) => (
                                            <React.Fragment key={key}>
                                                {key === 0 && (
                                                    <li className="page-item">
                                                        <Button
                                                            variant="link"
                                                            className={
                                                                pageIndex ===
                                                                item
                                                                    ? "page-link active"
                                                                    : "page-link"
                                                            }
                                                            onClick={() =>
                                                                gotoPage(item)
                                                            }
                                                        >
                                                            {item + 1}
                                                        </Button>
                                                    </li>
                                                )}
                                                {key > 0 &&
                                                    key <
                                                        pageOptions.length -
                                                            1 && (
                                                        <React.Fragment>
                                                            {Math.abs(
                                                                pageIndex - item
                                                            ) <= 1 && (
                                                                <li className="page-item">
                                                                    <Button
                                                                        variant="link"
                                                                        className={
                                                                            pageIndex ===
                                                                            item
                                                                                ? "page-link active"
                                                                                : "page-link"
                                                                        }
                                                                        onClick={() =>
                                                                            gotoPage(
                                                                                item
                                                                            )
                                                                        }
                                                                    >
                                                                        {item +
                                                                            1}
                                                                    </Button>
                                                                </li>
                                                            )}
                                                            {Math.abs(
                                                                pageIndex - item
                                                            ) > 1 &&
                                                                key ===
                                                                    Math.floor(
                                                                        pageOptions.length /
                                                                            2
                                                                    ) && (
                                                                    <li className="page-item disabled">
                                                                        <span className="page-link">
                                                                            ...
                                                                        </span>
                                                                    </li>
                                                                )}
                                                        </React.Fragment>
                                                    )}
                                                {key ===
                                                    pageOptions.length - 1 && (
                                                    <li className="page-item">
                                                        <Button
                                                            variant="link"
                                                            className={
                                                                pageIndex ===
                                                                item
                                                                    ? "page-link active"
                                                                    : "page-link"
                                                            }
                                                            onClick={() =>
                                                                gotoPage(item)
                                                            }
                                                        >
                                                            {item + 1}
                                                        </Button>
                                                    </li>
                                                )}
                                            </React.Fragment>
                                        )
                                    )}
                                </ul>
                            )}
                            <li
                                className={
                                    !canNextPage
                                        ? "page-item disabled"
                                        : "page-item"
                                }
                                onClick={nextPage}
                            >
                                <Button variant="link" className="page-link">
                                    Next
                                </Button>
                            </li>
                        </ul>
                    </div>
                </Row>
            )}
        </Fragment>
    );
};

TableContainer.propTypes = {
    preGlobalFilteredRows: PropTypes.any,
};

export default TableContainer;
