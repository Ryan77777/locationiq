import Layout from "@common/Layout";
import MostSearched from "@component/Dashboard/MostSearched";
import SearchByOperator from "@component/Dashboard/SearchByOperator";
import SearchHistory from "@component/Dashboard/SearchHistory";
import Head from "next/head";
import React, { ReactElement } from "react";
import { Col, Container, Row } from "react-bootstrap";

const Dashboard = () => {
    return (
        <React.Fragment>
            <Head>
                <title>Dashboard | 0320 Xplorer</title>
            </Head>
            <div className="page-content">
                <Container fluid>
                    <Row>
                        <Col>
                            <div className="h-100">
                                <MostSearched />
                                <Row>
                                    <SearchByOperator />
                                    <SearchHistory />
                                </Row>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        </React.Fragment>
    );
};

Dashboard.getLayout = (page: ReactElement) => {
    return <Layout>{page}</Layout>;
};

export default Dashboard;
