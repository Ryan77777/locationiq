import { APIClient } from "Components/helpers/api_helper";
import {
    apiError,
    setLoading,
    storeDataKTP,
    storeDataKTPStatistic,
    storeTotalKTP,
} from "./reducer";
import axios from "axios";

const BASE_URL = process.env.NEXT_PUBLIC_BASE_URL_AUTH;

const API = new APIClient();

export const getAllDataKTP = () => async (dispatch: any) => {
    setLoading(true);
    try {
        const response: any = await API.get("/data-kpu");

        if (response) {
            setLoading(false);
            dispatch(storeDataKTP(response));
        }
    } catch (error: any) {
        setLoading(false);
        dispatch(apiError("No data founds"));
    }
};

export const getDataKTPStatistic = () => async (dispatch: any) => {
    setLoading(true);
    try {
        const response: any = await axios.get(
            `${BASE_URL}/ktp-update-statistic/`
        );

        if (response.length > 0) {
            dispatch(
                storeDataKTPStatistic({
                    total_data_updated: response[0].total_data_updated,
                    last_updated_at: response[0].last_updated_at,
                })
            );
        }
    } catch (error: any) {
        setLoading(false);
        dispatch(apiError("No data founds"));
    }
};

export const getTotalData = () => async (dispatch: any) => {
    setLoading(true);
    try {
        const response: any = await API.get(`/data-kpu-count/`);

        if (response) {
            dispatch(
                storeTotalKTP({
                    total_ktp: response.totalCount,
                })
            );
        }
    } catch (error: any) {
        setLoading(false);
        dispatch(apiError("No data founds"));
    }
};
