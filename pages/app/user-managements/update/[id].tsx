import Layout from "@common/Layout";
import { changeToastAction } from "Components/slices/utils/reducer";
import { useGetRole } from "Hooks/useRole";
import { useGetUser, useUpdateUser } from "Hooks/useUserManagment";
import dynamic from "next/dynamic";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { ReactElement, useEffect, useState } from "react";
import { Button, Card, Col, Container, Form, Row } from "react-bootstrap";
import { useDispatch } from "react-redux";
const Select = dynamic(() => import("react-select"), { ssr: false });

const UpdateUser = () => {
    const router = useRouter();
    const dispatch = useDispatch();
    const userId = router.query.id;

    const { data: userData } = useGetUser();
    const { mutateAsync } = useUpdateUser();

    const { data: roles } = useGetRole();
    const [roleOptions, setRoleOptions] = useState<any[]>([]);

    useEffect(() => {
        if (roles) {
            setRoleOptions(
                roles.map((role: any) => ({
                    value: role.name,
                    label: role.name,
                }))
            );
        }
    }, [roles]);

    const [isLoading, setIsLoading] = useState(false);
    const [email, setEmail] = useState("");
    const [name, setName] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [role, setRole] = useState("");

    const onSubmit = async () => {
        setIsLoading(true);
        try {
            var dataSubmit: any = {
                userId,
                email,
                name,
                username,
                role,
                status: true,
            };

            if (password !== "") {
                dataSubmit.password = password;
            }

            const response: any = await mutateAsync(dataSubmit);

            if (response) {
                setTimeout(() => {
                    setIsLoading(false);
                    dispatch(changeToastAction("update"));
                    router.push("/app/user-managements");
                }, 1000);
            }
        } catch (error: any) {
            console.error("Error creating group:", error);
        }
    };

    useEffect(() => {
        if (userData) {
            const user =
                userData &&
                userData?.filter((item: any) => item.userId === userId)?.[0];

            setEmail(user?.email);
            setName(user?.name);
            setUsername(user?.username);
            setRole(user?.role);
        }
    }, [userData, userId]);

    return (
        <React.Fragment>
            <Head>
                <title>Update User | 0320 Xplorer</title>
            </Head>
            {userData !== undefined ? (
                <div className="page-content">
                    <Container fluid>
                        <Row>
                            <Col lg={12}>
                                <Card>
                                    <Card.Header>
                                        <h5 className="card-title mb-0">
                                            Updat User
                                        </h5>
                                    </Card.Header>
                                    <Card.Body>
                                        <Row className="">
                                            <Col lg={12} className="mb-4">
                                                <Form.Label>Role</Form.Label>
                                                {roles && roleOptions ? (
                                                    <Select
                                                        name="interval"
                                                        className="basic-single"
                                                        classNamePrefix="select"
                                                        defaultValue={
                                                            roleOptions[
                                                                roleOptions.findIndex(
                                                                    (it) =>
                                                                        it.value ==
                                                                        role
                                                                )
                                                            ]
                                                        }
                                                        options={roleOptions}
                                                        onChange={(e: any) =>
                                                            setRole(e.value)
                                                        }
                                                    />
                                                ) : (
                                                    "Loading.."
                                                )}
                                            </Col>
                                            <Col lg={6} className="mb-4">
                                                <Form.Label>Name</Form.Label>
                                                <Form.Control
                                                    type="text"
                                                    placeholder="Input here..."
                                                    value={name}
                                                    onChange={(e) =>
                                                        setName(e.target.value)
                                                    }
                                                />
                                            </Col>
                                            <Col lg={6} className="mb-4">
                                                <Form.Label>Email</Form.Label>
                                                <Form.Control
                                                    type="text"
                                                    placeholder="Input here..."
                                                    value={email}
                                                    onChange={(e) =>
                                                        setEmail(e.target.value)
                                                    }
                                                />
                                            </Col>
                                        </Row>
                                        <Row className="">
                                            <Col lg={6} className="mb-4">
                                                <Form.Label>
                                                    Username
                                                </Form.Label>
                                                <Form.Control
                                                    type="text"
                                                    placeholder="Input here..."
                                                    value={username ?? ""}
                                                    onChange={(e) =>
                                                        setUsername(
                                                            e.target.value
                                                        )
                                                    }
                                                />
                                            </Col>
                                            <Col lg={6} className="mb-4">
                                                <Form.Label>
                                                    Password
                                                </Form.Label>
                                                <Form.Control
                                                    type="text"
                                                    placeholder="Input here..."
                                                    value={password ?? ""}
                                                    onChange={(e) =>
                                                        setPassword(
                                                            e.target.value
                                                        )
                                                    }
                                                />
                                            </Col>
                                            <Col lg={12}>
                                                <Button
                                                    className="btn-primary"
                                                    onClick={onSubmit}
                                                >
                                                    Update User
                                                </Button>
                                            </Col>
                                        </Row>
                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>
                    </Container>

                    {isLoading && (
                        <div
                            style={{
                                background: "rgba(0, 0, 0, 0.5)",
                                position: "absolute",
                                top: 0,
                                bottom: 0,
                                left: 0,
                                right: 0,
                                zIndex: 9999999,
                            }}
                        >
                            <div id="status">
                                <div className="spinner-border text-primary avatar-sm">
                                    <span className="visually-hidden">
                                        Loading...
                                    </span>
                                </div>
                            </div>
                        </div>
                    )}
                </div>
            ) : (
                <div>Loading...</div>
            )}
        </React.Fragment>
    );
};

UpdateUser.getLayout = (page: ReactElement) => {
    return <Layout>{page}</Layout>;
};

export default UpdateUser;
