import Head from "next/head";
import { ReactElement, ReactNode } from "react";
import { Provider } from "react-redux";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "../Components/assets/scss/themes.scss";
import { persistor, store } from "../Components/slices";

import type { NextComponentType, NextPage } from "next";
import {
    AppContext,
    AppInitialProps,
    AppLayoutProps,
    AppProps,
} from "next/app";

import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { PersistGate } from "redux-persist/integration/react";

type NextPageWithLayout = NextPage & {
    getLayout?: (page: ReactElement) => ReactNode;
};

type AppPropsWithLayout = AppProps & {
    Component: NextPageWithLayout;
};
const MyApp: NextComponentType<AppContext, AppInitialProps, AppLayoutProps> = ({
    Component,
    pageProps,
    ...rest
}: AppPropsWithLayout) => {
    const getLayout = Component.getLayout || ((page) => page);

    const queryClient = new QueryClient();

    return (
        <>
            <Head>
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
                <link rel="icon" href="/favicon.ico" sizes="any" />

                <title>0320 Xplorer</title>
            </Head>
            <QueryClientProvider client={queryClient}>
                <Provider store={store}>
                    <PersistGate persistor={persistor} loading={null}>
                        <ToastContainer autoClose={1000} />
                        {getLayout(<Component {...pageProps} />)}
                    </PersistGate>
                </Provider>
            </QueryClientProvider>
        </>
    );
};

export default MyApp;
