import React, { useEffect, useState } from "react";
import { Card, Col, ProgressBar, Row } from "react-bootstrap";

import { APIClient } from "Components/helpers/api_helper";

function randomColor() {
    const letters = "0123456789ABCDEF";
    let color = "#";

    for (let i = 0; i < 6; i++) {
        const randomIndex = Math.floor(Math.random() * 16);
        color += letters[randomIndex];
    }

    return color;
}

const MostSearched = () => {
    const API = new APIClient();

    const [data, setData] = useState<any>();
    const [total, setTotal] = useState<any>();

    const getData = async () => {
        try {
            const obj = JSON.parse(localStorage.getItem("authUser") || "");
            const response: any = await API.get(`/stat-by-city/${obj.id}`);

            const filteredData = response.filter(
                (item: any) => item.cityName !== "" && item.cityName !== " "
            );

            const newBars = filteredData.map((item: any) => ({
                name: item.cityName,
                count: item.count,
                color: randomColor(),
            }));

            newBars.sort((a: any, b: any) => b.count - a.count);

            setData(newBars);

            const total = newBars.reduce(
                (accumulator: any, item: any) => accumulator + item.count,
                0
            );

            setTotal(total);
        } catch (error) {
            console.log(error);
        }
    };

    useEffect(() => {
        getData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <React.Fragment>
            <Col xl={12}>
                <Card className="card-height-100">
                    <Card.Header className="align-items-center d-flex">
                        <h4 className="card-title mb-0 flex-grow-1">
                            Most searched
                        </h4>
                    </Card.Header>

                    <Card.Body>
                        <Row>
                            <Col lg={12}>
                                <div>
                                    <p className="mb-2 fw-medium">
                                        Current Activity
                                    </p>

                                    <ProgressBar className="mb-4">
                                        {data?.map(
                                            (item: any, index: number) => (
                                                <ProgressBar
                                                    key={index}
                                                    style={{
                                                        backgroundColor:
                                                            item.color,
                                                    }}
                                                    now={
                                                        (item.count * 100) /
                                                        total
                                                    }
                                                />
                                            )
                                        )}
                                    </ProgressBar>

                                    {data?.map((item: any, index: number) => (
                                        <p key={index}>
                                            <i
                                                style={{
                                                    color: item.color,
                                                }}
                                                className="ri-checkbox-blank-circle-fill align-bottom me-1"
                                            ></i>
                                            {item.name}
                                            <span className="float-end">
                                                {Math.floor(
                                                    (item.count * 100) / total
                                                )}
                                                %
                                            </span>
                                        </p>
                                    ))}
                                </div>
                            </Col>
                        </Row>
                    </Card.Body>
                </Card>
            </Col>
        </React.Fragment>
    );
};

export default MostSearched;
