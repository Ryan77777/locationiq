import Layout from "@common/Layout";
import Table from "@component/LeakKTP/Table";
import {
    getAllDataKTP,
    getDataKTPStatistic,
    getTotalData,
} from "Components/slices/ktp/thunk";
import moment from "moment";
import Head from "next/head";
import React, { ReactElement, useEffect } from "react";
import { Card, Col, Container, Row } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";

const LeakKTP = () => {
    const dispatch: any = useDispatch();
    const { total_data_updated, last_updated_at, total_ktp } = useSelector(
        (state: any) => state.KTP
    );

    const getData = () => {
        dispatch(getAllDataKTP());
        dispatch(getDataKTPStatistic());
        dispatch(getTotalData());
    };

    useEffect(() => {
        getData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <React.Fragment>
            <Head>
                <title>Data KTP | 0320 Xplorer</title>
            </Head>
            <div className="page-content">
                <Container fluid>
                    <Row>
                        <Col xl={4} md={6}>
                            <Card className="card-animate">
                                <Card.Body>
                                    <div className="d-flex justify-content-between">
                                        <div className="flex-grow-1">
                                            <p className="text-uppercase fw-medium text-muted text-truncate fs-13">
                                                Total Data KTP
                                            </p>
                                            <div className="d-flex align-items-center gap-2">
                                                <h5 className="text-success mb-0">
                                                    {total_ktp.toLocaleString(
                                                        "id-ID"
                                                    )}
                                                </h5>
                                                <p className="text-muted mb-0">
                                                    Data
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col xl={4} md={6}>
                            <Card className="card-animate">
                                <Card.Body>
                                    <div className="d-flex justify-content-between">
                                        <div className="flex-grow-1">
                                            <p className="text-uppercase fw-medium text-muted text-truncate fs-13">
                                                Total Data Update
                                            </p>
                                            <div className="d-flex align-items-center gap-2">
                                                <h5 className="text-success mb-0">
                                                    {total_data_updated}
                                                </h5>
                                                <p className="text-muted mb-0">
                                                    Data
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col xl={4} md={6}>
                            <Card className="card-animate">
                                <Card.Body>
                                    <div className="d-flex justify-content-between">
                                        <div className="flex-grow-1">
                                            <p className="text-uppercase fw-medium text-muted text-truncate fs-13">
                                                Last Update KTP
                                            </p>
                                            <div className="d-flex align-items-center gap-2">
                                                <h5 className="text-success mb-0">
                                                    {moment(
                                                        last_updated_at
                                                    ).format(
                                                        "MM/DD/YYYY HH:mm"
                                                    ) ?? "-"}
                                                </h5>
                                            </div>
                                        </div>
                                    </div>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                    <Row>
                        <Col lg={12}>
                            <Card>
                                <Card.Header>
                                    <h5 className="card-title mb-0">
                                        Data KTP
                                    </h5>
                                </Card.Header>
                                <Card.Body>
                                    <Table />
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
        </React.Fragment>
    );
};

LeakKTP.getLayout = (page: ReactElement) => {
    return <Layout>{page}</Layout>;
};

export default LeakKTP;
