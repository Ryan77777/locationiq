import { createSlice } from "@reduxjs/toolkit";

export const initialState = {
    user: "",
    error: "",
};

const loginSlice = createSlice({
    name: "login",
    initialState,
    reducers: {
        apiError(state, action) {
            state.error = action.payload;
        },
        loginSuccess(state, action) {
            state.user = action.payload;
            state.error = "";
        },
        reset_login_flag(state) {
            state.error = "";
        },
        logoutSuccess(state) {
            state.user = "";
            state.error = "";
        },
    },
});

export const { apiError, loginSuccess, reset_login_flag, logoutSuccess } =
    loginSlice.actions;

export default loginSlice.reducer;
