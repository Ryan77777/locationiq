import {
    Circle,
    GoogleMap,
    Marker,
    Polygon,
    useLoadScript,
} from "@react-google-maps/api";
import axios from "axios";
import { useRouter } from "next/router";
import { useCallback, useEffect, useMemo, useRef, useState } from "react";
import { Badge, Button, Card } from "react-bootstrap";

type MapOptions = google.maps.MapOptions;

const BASE_URL = process.env.NEXT_PUBLIC_BASE_URL;

function formatDateTime(timestamp: any) {
    const date = new Date(timestamp * 1000);
    const formattedDate = date.toISOString().slice(0, 19).replace("T", " ");
    return formattedDate;
}

const Maps = () => {
    const router = useRouter();

    const { id } = router.query;

    const mapRef = useRef<any>(null);

    const [target, setTarget] = useState<any>({});
    const [rectangle, setRectangle] = useState<any>([]);
    const [detail, setDetail] = useState<any>({});

    const [message, setMessage] = useState("Memuat data...");

    // Fetching
    const getData = async () => {
        try {
            const AUTH_KEY = JSON.parse(
                localStorage.getItem("authUser") || ""
            ).token;

            const response: any = await axios.get(
                `${BASE_URL}/geofencing-log/jobId/${id}`,
                {
                    headers: {
                        Authorization: `Bearer ${AUTH_KEY}`,
                    },
                }
            );

            if (response.length > 0) {
                const res = response?.slice(-1)[0];

                setTarget(res.lastPosition);
                setRectangle(res.perimeter);
                setDetail({
                    name: res.phoneName ?? "-",
                    phone: res.phoneId ?? "-",
                    last_updated: formatDateTime(res.lastUpdated),
                    status:
                        res.message === "Target is trespassing Out"
                            ? "Outside Area"
                            : "Inside Area",
                });
            } else {
                setMessage(
                    "Belum ada data. Anda akan kembali ke halaman sebelumnya dalam 3 detik"
                );
                setTimeout(() => {
                    router.back();
                }, 3000);
            }
        } catch (error) {
            console.log(error);
        }
    };
    //

    const { isLoaded } = useLoadScript({
        googleMapsApiKey: process.env.NEXT_PUBLIC_GOOGLE_MAP_KEY!,
    });

    const options = useMemo<MapOptions>(
        () => ({
            mapId: "122be4461fa39403",
            disableDefaultUI: true,
            clickableIcons: false,
        }),
        []
    );

    const polygonOptions = {
        fillOpacity: 0.3,
        fillColor: "#ff0000",
        strokeColor: "#ff0000",
        strokeWeight: 1,
    };

    const onLoad = useCallback(async (map: any) => {
        mapRef.current = map;
    }, []);

    const onUnmount = useCallback(() => {
        mapRef.current = null;
    }, []);

    useEffect(() => {
        if (id) {
            getData();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [router]);

    return isLoaded && target && target.lat && target.lng ? (
        <div style={{ height: "100vh", position: "relative" }}>
            <GoogleMap
                ref={mapRef}
                mapContainerStyle={{ height: "100%", width: "100%" }}
                zoom={13}
                center={target}
                options={options}
                onLoad={onLoad}
                onUnmount={onUnmount}
            >
                {rectangle.length > 0 && (
                    <Polygon paths={rectangle} options={polygonOptions} />
                )}

                <Circle
                    center={target}
                    radius={1000}
                    options={{
                        strokeColor: "green",
                        fillColor: "green",
                    }}
                />
                <Marker
                    position={target}
                    icon={{
                        url: "https://maps.google.com/mapfiles/ms/icons/red-dot.png",
                    }}
                    animation={window.google.maps.Animation.BOUNCE}
                />
            </GoogleMap>

            {/* Detail */}
            <Card
                className="card-custom"
                style={{
                    position: "absolute",
                    top: 24,
                    left: 88,
                    right: 24,
                    background: "rgba(0, 0, 0, 0.1)",
                    boxShadow: "0px 0px 10px rgba(255, 255, 255, 0.4)",
                }}
            >
                <Card.Body>
                    <span className="text-white text-start">
                        Name: {detail?.name}
                    </span>
                    <br />
                    <span className="text-white text-start">
                        Phone Number: {detail?.phone}
                    </span>
                    <br />
                    <span className="text-white text-start">
                        Last updated: {detail?.last_updated}
                    </span>
                    <br />
                    <span className="text-white text-start">
                        Location Status:{" "}
                        <Badge bg="success">{detail?.status}</Badge>
                    </span>
                </Card.Body>
            </Card>

            <Button
                variant="outline-light"
                onClick={() => router.back()}
                className="btn-icon"
                style={{
                    position: "absolute",
                    top: 24,
                    left: 24,
                    right: 24,
                    background: "rgba(0, 0, 0, 0.1)",
                    boxShadow: "0px 0px 10px rgba(255, 255, 255, 0.4)",
                    borderRadius: 999,
                }}
            >
                <i className="ri-arrow-left-line"></i>
            </Button>
        </div>
    ) : (
        <div
            style={{
                position: "absolute",
                top: "50%",
                left: "50%",
                transform: "translate(-50%, -50%)",
                textAlign: "center",
            }}
        >
            {message}
        </div>
    );
};

function getColorForIndex(index: number) {
    const colors = ["red", "blue", "green", "yellow"];
    return colors[index % colors.length];
}

export default Maps;
