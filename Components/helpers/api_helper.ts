import axios from "axios";

// default
axios.defaults.baseURL = process.env.NEXT_PUBLIC_BASE_URL;

// content type
axios.defaults.headers.post["Content-Type"] = "application/json";

axios.interceptors.request.use(
    (config) => {
        // Add auth token to request headers
        const AUTH_KEY = JSON.parse(
            localStorage.getItem("authUser") || ""
        )?.token;

        if (AUTH_KEY) {
            config.headers.Authorization = `Bearer ${AUTH_KEY}`;
        }

        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);

// intercepting to capture errors
axios.interceptors.response.use(
    function (response) {
        return response.data ? response.data : response;
    },
    function (error) {
        let message;
        switch (error.response.data.status) {
            case 500:
                message = "Internal Server Error";
                break;
            case 404:
                message =
                    "Sorry! the data you are looking for could not be found";
                break;
            case 403:
                localStorage.clear();
                message = "Forbidden Access";
                break;
            case 401:
                message = "Invalid credentials";
                break;
            default:
                message = error.message || error;
        }
        return Promise.reject(message);
    }
);

class APIClient {
    /**
     * Fetches data from given url
     */

    //  get = (url, params) => {
    //   return axios.get(url, params);
    // };
    get = (url: any, params?: any) => {
        let response;

        let paramKeys: any = [];

        if (params) {
            Object.keys(params).map((key) => {
                paramKeys.push(key + "=" + params[key]);
                return paramKeys;
            });

            const queryString =
                paramKeys && paramKeys.length ? paramKeys.join("&") : "";
            response = axios.get(`${url}?${queryString}`, params);
        } else {
            response = axios.get(`${url}`, params);
        }

        return response;
    };
    /**
     * post given data to url
     */
    post = (url: any, data: any) => {
        return axios.post(url, data);
    };
    /**
     * Updates data
     */
    update = (url: any, data: any) => {
        return axios.patch(url, data);
    };

    put = (url: any, data: any) => {
        return axios.put(url, data);
    };
    /**
     * Delete
     */
    delete = (url: any, config?: any) => {
        return axios.delete(url, { ...config });
    };
}
const getLoggedinUser = () => {
    const user =
        typeof window !== "undefined" && localStorage.getItem("authUser");
    if (!user) {
        return null;
    } else {
        return JSON.parse(user);
    }
};

export { APIClient, getLoggedinUser };
