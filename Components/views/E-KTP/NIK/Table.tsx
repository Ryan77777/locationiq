import TableContainer from "@common/TableContainer";
import React, { useMemo } from "react";
import Image from "next/image";

const Table: React.FC<any> = ({ data }) => {
    const columns = useMemo(
        () => [
            {
                Header: "No.",
                Cell: ({ row }: any) => row.index + 1,
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "Nama",
                accessor: (cellProps: any) => {
                    return (
                        <div className="flex">
                            <Image
                                src={`data:image/png;base64,${cellProps.FOTO}`}
                                alt="Photo"
                                width={36}
                                height={36}
                                style={{ borderRadius: 36, marginRight: 8 }}
                            />
                            <span>{cellProps.NAMA_LGKP}</span>
                        </div>
                    );
                },
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "Status",
                accessor: "STAT_HBKEL",
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "Tanggal Lahir",
                accessor: "TGL_LHR",
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "Tempat Lahir",
                accessor: "TMPT_LHR",
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "Nama Ayah",
                accessor: "NAMA_LGKP_AYAH",
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "Nama Ibu",
                accessor: "NAMA_LGKP_IBU",
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "NIK",
                accessor: "NIK",
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "NKK",
                accessor: "NKK",
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "Jenis Kelamin",
                accessor: "JENIS_KLMIN",
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "Alamat",
                accessor: "ALAMAT",
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "Kawin",
                accessor: "STAT_KWN",
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "Pekerjaan",
                accessor: "JENIS_PKRJN",
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "Pendidikan Akhir",
                accessor: "PDDK_AKH",
                disableFilters: true,
                filterable: false,
            },
        ],
        []
    );

    return (
        <React.Fragment>
            <TableContainer
                columns={columns}
                data={data}
                isPagination
                isBordered
                customPageSize={10}
                className="table align-middle table-nowrap"
                tableClass="table-centered align-middle table-nowrap mb-0"
                theadClass="text-muted table-light"
                SearchPlaceholder="Search..."
            />
        </React.Fragment>
    );
};

export default Table;
