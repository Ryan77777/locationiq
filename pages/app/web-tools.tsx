import Layout from "@common/Layout";
import Head from "next/head";
import React, { ReactElement } from "react";
import { Col, Container, Row } from "react-bootstrap";

const WebTools = () => {
    return (
        <React.Fragment>
            <Head>
                <title>Dashboard | 0320 Xplorer</title>
            </Head>
            <div className="page-content">
                <Container fluid>
                    <Row>
                        <Col>text</Col>
                    </Row>
                </Container>
            </div>
        </React.Fragment>
    );
};

WebTools.getLayout = (page: ReactElement) => {
    return <Layout>{page}</Layout>;
};

export default WebTools;
