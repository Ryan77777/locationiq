import Layout from "@common/Layout";
import { changeToastAction } from "Components/slices/utils/reducer";
import { useGetRole } from "Hooks/useRole";
import { useCreateUser } from "Hooks/useUserManagment";
import dynamic from "next/dynamic";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { ReactElement, useState } from "react";
import { Button, Card, Col, Container, Form, Row } from "react-bootstrap";
import { useDispatch } from "react-redux";
const Select = dynamic(() => import("react-select"), { ssr: false });

const CreateUser = () => {
	const router = useRouter();
	const dispatch = useDispatch();

	const { data: roles } = useGetRole();
    const { mutateAsync } = useCreateUser();

	const roleOptions =
		roles &&
		roles.map((role: any) => ({
			value: role.name,
			label: role.name,
		}));

	const [isLoading, setIsLoading] = useState(false);
	const [email, setEmail] = useState("");
	const [name, setName] = useState("");
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const [role, setRole] = useState("");

	const onSubmit = async () => {
		setIsLoading(true);
		try {
			const data = {
				email,
				name,
				username,
				password,
				role,
				status: true,
			};

			const response: any = await mutateAsync(data);

			if (response) {
			    setTimeout(() => {
			        setIsLoading(false);
			        dispatch(changeToastAction("create"));
			        router.push("/app/user-managements");
			    }, 1000);
			}
		} catch (error: any) {
			console.error("Error creating group:", error);
		}
	};

	return (
		<React.Fragment>
			<Head>
				<title>User | 0320 Xplorer</title>
			</Head>
			<div className="page-content">
				<Container fluid>
					<Row>
						<Col lg={12}>
							<Card>
								<Card.Header>
									<h5 className="card-title mb-0">
										Create New User
									</h5>
								</Card.Header>
								<Card.Body>
									<Row className="">
										<Col lg={12} className="mb-4">
											<Form.Label>Role</Form.Label>
											<Select
												name="interval"
												className="basic-single"
												classNamePrefix="select"
												defaultValue="Select Interval"
												options={roleOptions}
												onChange={(e: any) =>
													setRole(e.value)
												}
											/>
										</Col>
										<Col lg={6} className="mb-4">
											<Form.Label>Name</Form.Label>
											<Form.Control
												type="text"
												placeholder="Input here..."
												value={name}
												onChange={(e) =>
													setName(e.target.value)
												}
											/>
										</Col>
										<Col lg={6} className="mb-4">
											<Form.Label>Email</Form.Label>
											<Form.Control
												type="text"
												placeholder="Input here..."
												value={email}
												onChange={(e) =>
													setEmail(e.target.value)
												}
											/>
										</Col>
									</Row>
									<Row className="">
										<Col lg={6} className="mb-4">
											<Form.Label>Username</Form.Label>
											<Form.Control
												type="text"
												placeholder="Input here..."
												value={username}
												onChange={(e) =>
													setUsername(e.target.value)
												}
											/>
										</Col>
										<Col lg={6} className="mb-4">
											<Form.Label>Password</Form.Label>
											<Form.Control
												type="text"
												placeholder="Input here..."
												value={password}
												onChange={(e) =>
													setPassword(e.target.value)
												}
											/>
										</Col>
										<Col lg={12}>
											<Button
												className="btn-primary"
												onClick={onSubmit}>
												Create User
											</Button>
										</Col>
									</Row>
								</Card.Body>
							</Card>
						</Col>
					</Row>
				</Container>

				{isLoading && (
					<div
						style={{
							background: "rgba(0, 0, 0, 0.5)",
							position: "absolute",
							top: 0,
							bottom: 0,
							left: 0,
							right: 0,
							zIndex: 9999999,
						}}>
						<div id="status">
							<div className="spinner-border text-primary avatar-sm">
								<span className="visually-hidden">
									Loading...
								</span>
							</div>
						</div>
					</div>
				)}
			</div>
		</React.Fragment>
	);
};

CreateUser.getLayout = (page: ReactElement) => {
	return <Layout>{page}</Layout>;
};

export default CreateUser;
