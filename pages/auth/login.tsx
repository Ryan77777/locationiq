import Head from "next/head";
import React, { useEffect, useState } from "react";
import {
    Alert,
    Button,
    Card,
    Col,
    Container,
    Form,
    Row,
    Spinner,
} from "react-bootstrap";
import ReCAPTCHA from "react-google-recaptcha";

import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";

import { useFormik } from "formik";
import * as Yup from "yup";

import { loginUser, resetLoginFlag } from "../../Components/slices/thunk";

//import images
import NonAuthLayout from "@common/Layout/NonAuthLayout";
import Image from "next/image";
import { createSelector } from "reselect";
import logosm from "../../Components/assets/images/logo.png";
import { apiError } from "Components/slices/auth/login/reducer";

const Login = () => {
    const dispatch: any = useDispatch();
    const router = useRouter();
    const [loading, setLoading] = useState<boolean>(false);
    const [passwordtype, setPasswordtype] = useState<boolean>(true);
    const [isVerify, setIsVerify] = useState<string>("");

    const selectLayoutState: any = (state: any) => state;

    const selectLayoutProperties = createSelector(
        selectLayoutState,
        (Login) => ({
            user: Login.Login.user,
            error: Login.Login.error,
        })
    );
    const { user, error } = useSelector(selectLayoutProperties);

    const validation: any = useFormik({
        enableReinitialize: true,

        initialValues: {
            username: user.username,
            password: user.password,
        },
        validationSchema: Yup.object({
            username: Yup.string().required("Please Enter Your Username"),
            password: Yup.string().required("Please Enter Your Password"),
        }),
        onSubmit: (values) => {
            setLoading(true);
            if (isVerify) {
                dispatch(loginUser(values, router));
            } else {
                setLoading(false);
                dispatch(apiError("Please check reCaptcha"));
            }
        },
    });

    useEffect(() => {
        setTimeout(() => {
            setLoading(false);
        }, 3000);
    }, [dispatch, error]);

    useEffect(() => {
        dispatch(resetLoginFlag());
    }, [dispatch]);

    return (
        <React.Fragment>
            <Head>
                <title>Login | 0320 Xplorer</title>
            </Head>
            <section className="auth-bg-cover position-relative d-flex align-items-center justify-content-center min-vh-100">
                <Container>
                    <Row className="d-flex justify-content-center align-align-items-center">
                        <Col lg={6}>
                            <Card>
                                <Card.Body>
                                    <Row>
                                        <Col lg={12}>
                                            <Card className="mb-0 border-0 shadow-none">
                                                <Card.Body className="px-0 p-sm-3 m-lg-2">
                                                    <div className="text-center mt-2">
                                                        <Image
                                                            priority
                                                            src={logosm}
                                                            alt="logo"
                                                            height="100"
                                                            className="mb-3"
                                                        />
                                                        <h5 className="text-primary fs-20">
                                                            Welcome Back !
                                                        </h5>
                                                        <p className="text-white">
                                                            Sign in to continue
                                                            to Dashboard.
                                                        </p>
                                                    </div>
                                                    {error ? (
                                                        <Alert variant="danger">
                                                            {error}
                                                        </Alert>
                                                    ) : null}
                                                    <div className="p-2 mt-3">
                                                        <Form
                                                            onSubmit={(e) => {
                                                                e.preventDefault();
                                                                validation.handleSubmit();
                                                                return false;
                                                            }}
                                                        >
                                                            <div className="mb-3">
                                                                <Form.Label
                                                                    htmlFor="username"
                                                                    className="form-label"
                                                                >
                                                                    Username
                                                                </Form.Label>
                                                                <Form.Control
                                                                    className="form-control"
                                                                    id="username"
                                                                    placeholder="Enter username"
                                                                    name="username"
                                                                    type="text"
                                                                    onChange={
                                                                        validation.handleChange
                                                                    }
                                                                    onBlur={
                                                                        validation.handleBlur
                                                                    }
                                                                    value={
                                                                        validation
                                                                            .values
                                                                            .username ||
                                                                        ""
                                                                    }
                                                                    isInvalid={
                                                                        validation
                                                                            .touched
                                                                            .username &&
                                                                        validation
                                                                            .errors
                                                                            .username
                                                                            ? true
                                                                            : false
                                                                    }
                                                                />
                                                                {validation
                                                                    .touched
                                                                    .username &&
                                                                validation
                                                                    .errors
                                                                    .username ? (
                                                                    <Form.Control.Feedback type="invalid">
                                                                        {
                                                                            validation
                                                                                .errors
                                                                                .username
                                                                        }
                                                                    </Form.Control.Feedback>
                                                                ) : null}
                                                            </div>

                                                            <div className="mb-3">
                                                                <Form.Label
                                                                    className="form-label"
                                                                    htmlFor="password-input"
                                                                >
                                                                    Password
                                                                </Form.Label>
                                                                <div className="position-relative auth-pass-inputgroup mb-3">
                                                                    <Form.Control
                                                                        type={
                                                                            passwordtype
                                                                                ? "password"
                                                                                : "text"
                                                                        }
                                                                        className="form-control pe-5 password-input"
                                                                        placeholder="Enter password"
                                                                        id="password-input"
                                                                        name="password"
                                                                        value={
                                                                            validation
                                                                                .values
                                                                                .password ||
                                                                            ""
                                                                        }
                                                                        onChange={
                                                                            validation.handleChange
                                                                        }
                                                                        onBlur={
                                                                            validation.handleBlur
                                                                        }
                                                                        isInvalid={
                                                                            validation
                                                                                .touched
                                                                                .password &&
                                                                            validation
                                                                                .errors
                                                                                .password
                                                                                ? true
                                                                                : false
                                                                        }
                                                                    />
                                                                    {validation
                                                                        .touched
                                                                        .password &&
                                                                    validation
                                                                        .errors
                                                                        .password ? (
                                                                        <Form.Control.Feedback type="invalid">
                                                                            {
                                                                                validation
                                                                                    .errors
                                                                                    .password
                                                                            }
                                                                        </Form.Control.Feedback>
                                                                    ) : (
                                                                        <Button
                                                                            variant="link"
                                                                            className="position-absolute end-0 top-0 text-decoration-none text-muted password-addon"
                                                                            type="button"
                                                                            id="password-addon"
                                                                            onClick={() =>
                                                                                setPasswordtype(
                                                                                    !passwordtype
                                                                                )
                                                                            }
                                                                        >
                                                                            <i className="ri-eye-fill align-middle text-white"></i>
                                                                        </Button>
                                                                    )}
                                                                </div>
                                                            </div>

                                                            <Row>
                                                                <Col sm={12}>
                                                                    <ReCAPTCHA
                                                                        sitekey="6LfX8U8iAAAAAIvvvOC01ufoXv4Fx7kNhY0qj4wk"
                                                                        onChange={(
                                                                            value: any
                                                                        ) =>
                                                                            setIsVerify(
                                                                                value
                                                                            )
                                                                        }
                                                                    />
                                                                </Col>
                                                            </Row>

                                                            <div className="mt-4">
                                                                <Button
                                                                    variant="primary"
                                                                    className="w-100"
                                                                    type="submit"
                                                                    disabled={
                                                                        loading
                                                                            ? true
                                                                            : false
                                                                    }
                                                                >
                                                                    {loading ? (
                                                                        <Spinner
                                                                            animation="border"
                                                                            size="sm"
                                                                            className="me-2"
                                                                        ></Spinner>
                                                                    ) : null}
                                                                    Sign In
                                                                </Button>
                                                            </div>
                                                        </Form>
                                                    </div>
                                                </Card.Body>
                                            </Card>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </section>
        </React.Fragment>
    );
};

Login.getLayout = function getLayout(page: any) {
    return <NonAuthLayout>{page}</NonAuthLayout>;
};

export default Login;
