import { logoutUser } from "Components/slices/thunk";
import Router, { useRouter } from "next/router";
import React, { useState } from "react";
import { useDispatch } from "react-redux";

const Navdata = () => {
    const dispatch: any = useDispatch();
    const router = useRouter();
    const [isMulti, setIsMulti] = useState(false);
    const [isCard, setIsCard] = useState(false);
    const [isTelco, setIsTelco] = useState(false);

    const menuItems: any = [
        {
            label: "Dashboard",
            isHeader: true,
        },
        {
            id: "dashboard",
            label: "Dashboard",
            icon: "bi bi-speedometer2",
            link: "/app/dashboard",
        },
        {
            label: "Main Features",
            isHeader: true,
        },
        {
            id: "singletarget",
            label: "Single Target",
            icon: "bi bi-person-fill",
            link: "/app/single-target",
        },
        {
            id: "multitarget",
            label: "Multi Target",
            icon: "bi bi-people-fill",
            link: "/#",
            click: function (e: any) {
                e.preventDefault();
                setIsMulti(!isMulti);
            },
            stateVariables: isMulti,
            subItems: [
                {
                    id: "geolocation",
                    label: "GeoLocation",
                    link: "/app/multi-target/geolocation",
                    parentId: "multitarget",
                    click: function (e: any) {
                        e.preventDefault();
                    },
                },
                {
                    id: "geofencing",
                    label: "GeoFencing",
                    link: "/app/multi-target/geofencing",
                    parentId: "multitarget",
                    click: function (e: any) {
                        e.preventDefault();
                    },
                },
            ],
        },
        {
            label: "Master Data",
            isHeader: true,
        },
        {
            id: "groups",
            label: "Groups",
            icon: "bi bi-people-fill",
            link: "/app/groups",
        },
        {
            id: "telco",
            label: "Telco Registration",
            icon: "bi bi-telephone-fill",
            link: "/#",
            click: function (e: any) {
                e.preventDefault();
                setIsTelco(!isTelco);
            },
            stateVariables: isTelco,
            subItems: [
                {
                    id: "niktomsisdn",
                    label: "NIK to MSISDN",
                    link: "/app/telco-registrations/nik-to-msisdn",
                    parentId: "multitarget",
                    click: function (e: any) {
                        e.preventDefault();
                    },
                },
                {
                    id: "msisdntonik",
                    label: "MSISDN to NIK",
                    link: "/app/telco-registrations/msisdn-to-nik",
                    parentId: "multitarget",
                    click: function (e: any) {
                        e.preventDefault();
                    },
                },
            ],
        },
        {
            id: "ektp",
            label: "E-KTP",
            icon: "bi bi-card-image",
            link: "/#",
            click: function (e: any) {
                e.preventDefault();
                setIsCard(!isCard);
            },
            stateVariables: isCard,
            subItems: [
                {
                    id: "nik",
                    label: "Search by NIK",
                    link: "/app/e-ktp/search-by-nik",
                    parentId: "multitarget",
                    click: function (e: any) {
                        e.preventDefault();
                    },
                },
                {
                    id: "nkk",
                    label: "Search by NKK",
                    link: "/app/e-ktp/search-by-nkk",
                    parentId: "multitarget",
                    click: function (e: any) {
                        e.preventDefault();
                    },
                },
            ],
        },
        {
            id: "samsat",
            label: "Samsat",
            icon: "bi bi-car-front",
            link: "/#",
            click: function (e: any) {
                e.preventDefault();
                setIsCard(!isCard);
            },
            stateVariables: isCard,
            subItems: [
                {
                    id: "nik",
                    label: "Search by NIK",
                    link: "/app/samsat/search-by-nik",
                    parentId: "multitarget",
                    click: function (e: any) {
                        e.preventDefault();
                    },
                },
                {
                    id: "nkk",
                    label: "Search by Vehicle Number",
                    link: "/app/samsat/search-by-vehicle",
                    parentId: "multitarget",
                    click: function (e: any) {
                        e.preventDefault();
                    },
                },
            ],
        },
        {
            id: "leakktp",
            label: "Leak KTP",
            icon: "bi bi-card-image",
            link: "/app/leak-ktp",
        },
        {
            id: "webtools",
            label: "Web Tools",
            icon: "bi bi-tools",
            link: "https://tools.0320.site/",
        },
        {
            id: "spidertools",
            label: "Spider Tools",
            icon: "bi bi-tools",
            link: "https://spider.0320.site/",
        },
        {
            label: "Konfigurations",
            isHeader: true,
        },
        {
            id: "usermanagements",
            label: "User Managements",
            icon: "bi bi-person-fill-gear",
            link: "/app/user-managements",
        },
        {
            id: "logactivities",
            label: "Log Activities",
            icon: "bi bi-clock-history",
            link: "/app/log-activities",
        },
        {
            label: "Authentications",
            isHeader: true,
        },
        {
            id: "logout",
            label: "Logout",
            icon: "mdi mdi-logout text-muted fs-16 align-middle me-1",
            link: "/#",
            click: function () {
                dispatch(logoutUser(router));
            },
        },
    ];
    return <React.Fragment>{menuItems}</React.Fragment>;
};
export default Navdata;
