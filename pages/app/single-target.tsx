import Layout from "@common/Layout";
import Head from "next/head";
import Image from "next/image";
import React, { ReactElement, useCallback, useEffect, useState } from "react";
import {
    Button,
    Card,
    Col,
    Container,
    Form,
    Modal,
    Row,
} from "react-bootstrap";

// Import Components
import ILError from "@assets/images/Gifs/1140-error-outline.gif";
import { useDispatch, useSelector } from "react-redux";

// Import Assets
import {
    resetDataAction,
    resetError,
    setLoading,
} from "Components/slices/single-target/reducer";
import { trackingUser } from "Components/slices/thunk";
import dynamic from "next/dynamic";
import Link from "next/link";
import { createSelector } from "reselect";

const Maps = dynamic(() => import("@component/SingleTarget/Maps"), {
    loading: () => (
        <div
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
            <p>A map is loading</p>
        </div>
    ),
    ssr: false,
});

const SingleTarget = () => {
    const dispatch: any = useDispatch();
    const [phone, setPhone] = useState<string>("");
    const [selected, setSelected] = useState<any>(null);

    const selectLayoutState: any = (state: any) => state;

    const selectLayoutProperties = createSelector(
        selectLayoutState,
        (state) => ({
            data: state.SingleTarget.data,
            error: state.SingleTarget.error,
            loading: state.SingleTarget.loading,
        })
    );

    const { data, error, loading } = useSelector(selectLayoutProperties);

    const [isVisible, setIsVisible] = useState(false);

    const toggleModal = useCallback(() => {
        setIsVisible(false);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const onPhoneChange = (e: any) => {
        setPhone(e.target.value);
    };

    const onShowDetail = (index: number) => {
        setSelected(data?.[index]);
    };

    const onSearch = useCallback((phone: string) => {
        const cleanedNumber = phone.replace(/[\s-]/g, "");
        dispatch(trackingUser(cleanedNumber));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const onReset = useCallback(() => {
        dispatch(setLoading());
        setTimeout(() => {
            dispatch(resetDataAction());
            setSelected(null);
            setPhone("");
        }, 1000);
    }, [dispatch]);

    useEffect(() => {
        if (error) {
            setIsVisible(true);
        }
    }, [error]);

    useEffect(() => {
        dispatch(resetError());
    }, [dispatch]);

    return (
        <React.Fragment>
            <Head>
                <title>Single Target | 0320 Xplorer</title>
            </Head>
            <div className="page-content">
                <Container fluid>
                    <Row>
                        <Col xs={12}>
                            <div className="card-wrapper">
                                {/* Form */}
                                <Card
                                    className="card-custom"
                                    style={{
                                        position: "absolute",
                                        top: 24,
                                        left: 24,
                                        background: "rgba(255, 255, 255, 0.7)",
                                        boxShadow:
                                            "0px 0px 10px rgba(0, 0, 0, 0.2)",
                                        zIndex: 1,
                                    }}
                                >
                                    <Card.Body>
                                        <Form.Control
                                            className="form-control"
                                            type="text"
                                            placeholder="62..."
                                            value={phone}
                                            onChange={onPhoneChange}
                                        />
                                        <Row className="mt-3">
                                            <Col>
                                                <Button
                                                    style={{
                                                        marginRight: "4px",
                                                    }}
                                                    className="btn-sm"
                                                    variant="warning"
                                                    onClick={() =>
                                                        onSearch(phone)
                                                    }
                                                >
                                                    Tracking Location
                                                </Button>

                                                <Button
                                                    className="btn-sm"
                                                    variant="primary"
                                                    onClick={onReset}
                                                >
                                                    Reset
                                                </Button>
                                            </Col>
                                        </Row>
                                    </Card.Body>
                                </Card>

                                <Maps
                                    data={data}
                                    isLoading={loading}
                                    onClick={(index: number) =>
                                        onShowDetail(index)
                                    }
                                />

                                {/* Detail */}
                                {selected && !error && (
                                    <Card className="card-custom-detail">
                                        <Card.Body className="text-dark text-xs">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <small>MSISDN</small>
                                                    </td>
                                                    <td>
                                                        <small className="px-3">
                                                            :
                                                        </small>
                                                    </td>
                                                    <td>
                                                        <small>
                                                            {selected?.msisdn ??
                                                                "-"}
                                                        </small>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <small>IMEI</small>
                                                    </td>
                                                    <td>
                                                        <small className="px-3">
                                                            :
                                                        </small>
                                                    </td>
                                                    <td>
                                                        <small>
                                                            {selected?.imei ??
                                                                "-"}
                                                        </small>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <small>IMSI</small>
                                                    </td>
                                                    <td>
                                                        <small className="px-3">
                                                            :
                                                        </small>
                                                    </td>
                                                    <td>
                                                        <small>
                                                            {selected?.imsi ??
                                                                "-"}
                                                        </small>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <small>Phone</small>
                                                    </td>
                                                    <td>
                                                        <small className="px-3">
                                                            :
                                                        </small>
                                                    </td>
                                                    <td>
                                                        <small>
                                                            {selected?.phone ??
                                                                "-"}
                                                        </small>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <small>CI</small>
                                                    </td>
                                                    <td>
                                                        <small className="px-3">
                                                            :
                                                        </small>
                                                    </td>
                                                    <td>
                                                        <small>
                                                            {selected?.ci ??
                                                                "-"}
                                                        </small>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <small>MCC</small>
                                                    </td>
                                                    <td>
                                                        <small className="px-3">
                                                            :
                                                        </small>
                                                    </td>
                                                    <td>
                                                        <small>
                                                            {selected?.mcc ??
                                                                "-"}
                                                        </small>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <small>MNC</small>
                                                    </td>
                                                    <td>
                                                        <small className="px-3">
                                                            :
                                                        </small>
                                                    </td>
                                                    <td>
                                                        <small>
                                                            {selected?.mnc ??
                                                                "-"}
                                                        </small>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <small>TAC</small>
                                                    </td>
                                                    <td>
                                                        <small className="px-3">
                                                            :
                                                        </small>
                                                    </td>
                                                    <td>
                                                        <small>
                                                            {selected?.tac ??
                                                                "-"}
                                                        </small>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <small>State</small>
                                                    </td>
                                                    <td>
                                                        <small className="px-3">
                                                            :
                                                        </small>
                                                    </td>
                                                    <td>
                                                        <small>
                                                            {selected?.state ??
                                                                "-"}
                                                        </small>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <small>Network</small>
                                                    </td>
                                                    <td>
                                                        <small className="px-3">
                                                            :
                                                        </small>
                                                    </td>
                                                    <td>
                                                        <small>
                                                            {selected?.network ??
                                                                "-"}
                                                        </small>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <small>
                                                            Antena Azimuth
                                                            Degree
                                                        </small>
                                                    </td>
                                                    <td>
                                                        <small className="px-3">
                                                            :
                                                        </small>
                                                    </td>
                                                    <td>
                                                        <small>
                                                            {selected?.antena_azimuth_degree ??
                                                                "-"}
                                                        </small>
                                                    </td>
                                                </tr>
                                                <tr
                                                    style={{
                                                        borderBottom:
                                                            "0.5px dashed",
                                                        paddingBottom: "8px",
                                                    }}
                                                >
                                                    <td>
                                                        <small>AOL</small>
                                                    </td>
                                                    <td>
                                                        <small className="px-3">
                                                            :
                                                        </small>
                                                    </td>
                                                    <td>
                                                        <small>
                                                            {selected?.aol ??
                                                                "-"}
                                                        </small>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td className="pt-2">
                                                        <small>Provider</small>
                                                    </td>
                                                    <td>
                                                        <small className="px-3">
                                                            :
                                                        </small>
                                                    </td>
                                                    <td>
                                                        <small>
                                                            {selected?.net_provider ??
                                                                "-"}
                                                        </small>
                                                    </td>
                                                </tr>
                                                <tr
                                                    style={{
                                                        borderBottom:
                                                            "0.5px dashed",
                                                        paddingBottom: "8px",
                                                    }}
                                                >
                                                    <td>
                                                        <small>HLR</small>
                                                    </td>
                                                    <td>
                                                        <small className="px-3">
                                                            :
                                                        </small>
                                                    </td>
                                                    <td>
                                                        <small>
                                                            {selected?.registered_hlr ??
                                                                "-"}
                                                        </small>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td className="pt-2">
                                                        <small>Address</small>
                                                    </td>
                                                    <td>
                                                        <small className="px-3">
                                                            :
                                                        </small>
                                                    </td>
                                                    <td>
                                                        <small>
                                                            {selected?.address ??
                                                                "-"}
                                                        </small>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <small>District</small>
                                                    </td>
                                                    <td>
                                                        <small className="px-3">
                                                            :
                                                        </small>
                                                    </td>
                                                    <td>
                                                        <small>
                                                            {selected?.district ??
                                                                "-"}
                                                        </small>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <small>Province</small>
                                                    </td>
                                                    <td>
                                                        <small className="px-3">
                                                            :
                                                        </small>
                                                    </td>
                                                    <td>
                                                        <small>
                                                            {selected?.province ??
                                                                "-"}
                                                        </small>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <small>
                                                            Subdistrict
                                                        </small>
                                                    </td>
                                                    <td>
                                                        <small className="px-3">
                                                            :
                                                        </small>
                                                    </td>
                                                    <td>
                                                        <small>
                                                            {selected?.subdistrict ??
                                                                "-"}
                                                        </small>
                                                    </td>
                                                </tr>
                                                <tr
                                                    style={{
                                                        borderBottom:
                                                            "0.5px dashed",
                                                        paddingBottom: "8px",
                                                    }}
                                                >
                                                    <td>
                                                        <small>City</small>
                                                    </td>
                                                    <td>
                                                        <small className="px-3">
                                                            :
                                                        </small>
                                                    </td>
                                                    <td>
                                                        <small>
                                                            {selected?.city ??
                                                                "-"}
                                                        </small>
                                                    </td>
                                                </tr>

                                                <tr
                                                    style={{
                                                        borderBottom:
                                                            "0.5px dashed",
                                                        paddingBottom: "8px",
                                                    }}
                                                >
                                                    <td className="pt-2">
                                                        <small>
                                                            Created Date
                                                        </small>
                                                    </td>
                                                    <td>
                                                        <small className="px-3">
                                                            :
                                                        </small>
                                                    </td>
                                                    <td>
                                                        <small>
                                                            {selected?.dateCreate ??
                                                                "-"}
                                                        </small>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td className="pt-2">
                                                        <small>
                                                            Google Map Url
                                                        </small>
                                                    </td>
                                                    <td>
                                                        <small className="px-3">
                                                            :
                                                        </small>
                                                    </td>
                                                    <td>
                                                        <small>
                                                            {selected?.gmap_url ? (
                                                                <Link
                                                                    target="_blank"
                                                                    href={
                                                                        selected?.gmap_url
                                                                    }
                                                                >
                                                                    <span>
                                                                        Open in
                                                                        new tab
                                                                    </span>
                                                                </Link>
                                                            ) : (
                                                                "-"
                                                            )}
                                                        </small>
                                                    </td>
                                                </tr>
                                            </table>
                                        </Card.Body>
                                    </Card>
                                )}
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>

            {loading && (
                <div
                    style={{
                        background: "rgba(0, 0, 0, 0.5)",
                        position: "absolute",
                        top: 0,
                        bottom: 0,
                        left: 0,
                        right: 0,
                        zIndex: 9999999,
                    }}
                >
                    <div id="status">
                        <div className="spinner-border text-primary avatar-sm">
                            <span className="visually-hidden">Loading...</span>
                        </div>
                    </div>
                </div>
            )}

            {error && (
                <Modal show={isVisible} onHide={toggleModal} centered>
                    <Modal.Body className="text-center p-5">
                        <Image
                            src={ILError}
                            style={{ width: "120px", height: "120px" }}
                            alt=""
                        />
                        <div className="mt-4">
                            <h3 className="mb-3">Oops, data not found!</h3>
                            <p className="text-muted mb-4">
                                The data was not successfully found. The phone
                                number may not be correct.
                            </p>
                        </div>
                    </Modal.Body>
                </Modal>
            )}
        </React.Fragment>
    );
};

SingleTarget.getLayout = (page: ReactElement) => {
    return <Layout>{page}</Layout>;
};

export default SingleTarget;
