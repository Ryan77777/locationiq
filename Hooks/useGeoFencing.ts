import {
    UseMutationResult,
    UseQueryResult,
    useMutation,
    useQuery,
} from "@tanstack/react-query";
import { APIClient } from "Components/helpers/api_helper";

const API = new APIClient();

export const useGetGeoFencing = (): UseQueryResult<any, Error> => {
    return useQuery<any, Error>({
        queryKey: ["geofencing"],
        queryFn: async () => {
            const response: any = await API.get("/geofencing");

            return response;
        },
        retry: true,
        refetchOnWindowFocus: true,
        refetchOnMount: true,
    });
};

export const useCreateGeoFencing = (): UseMutationResult<any, Error, any> => {
    return useMutation({
        mutationFn: async (data) => {
            const response: any = await API.post("/geofencing", data);

            return response;
        },
    });
};

export const useUpdateGeoFencing = (): UseMutationResult<any, Error, any> => {
    return useMutation({
        mutationFn: async (data) => {
            const response: any = await API.put("/geofencing", data);

            return response;
        },
    });
};

export const useDeleteGeoFencing = (): UseMutationResult<any, Error, any> => {
    return useMutation({
        mutationFn: async (id: string) => {
            const AUTH_KEY = JSON.parse(
                localStorage.getItem("authUser") || ""
            ).token;
            const response: any = await API.delete(`/geofencing/${id}`);

            return response;
        },
    });
};
