import { createSlice } from "@reduxjs/toolkit";
//constants

export const initialState = {
    data: [],
    error: "",
    loading: false,
};

const SingleTargetSlice = createSlice({
    name: "SingleTargetSlice",
    initialState,
    reducers: {
        apiError(state, action) {
            state.error = action.payload;
            state.loading = false;
        },
        storeDataAction(state: any, action) {
            const newMsisdn = action.payload.msisdn;
            const isMsisdnExists = state.data.some(
                (item: any) => item.msisdn === newMsisdn
            );

            if (!isMsisdnExists) {
                state.data.push(action.payload);
            }

            state.loading = false;
        },
        setLoading(state: any) {
            state.loading = true;
        },
        resetError(state) {
            state.error = "";
        },
        resetDataAction(state) {
            state.data = [];
            state.loading = false;
        },
    },
});

export const {
    storeDataAction,
    apiError,
    setLoading,
    resetError,
    resetDataAction,
} = SingleTargetSlice.actions;

export default SingleTargetSlice.reducer;
