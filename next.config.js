/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: false,
    swcMinify: true,
    serverRuntimeConfig: {
        headers: {
            "Content-Security-Policy": "frame-ancestors 'none';",
        },
    },
    images: {
        domains: ["ui-avatars.com"],
    },
};

module.exports = nextConfig;
