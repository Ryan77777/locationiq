import axios from "axios";
import dynamic from "next/dynamic";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Button, Card, Col, Row } from "react-bootstrap";

const BASE_URL = process.env.NEXT_PUBLIC_BASE_URL;

function formatDateTime(timestamp: any) {
    const date = new Date(timestamp * 1000);
    const formattedDate = date.toISOString().slice(0, 19).replace("T", " ");
    return formattedDate;
}

const RenderMap = dynamic(() => import("@component/GeoLocation/Maps"), {
    loading: () => (
        <div
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
            <p>A map is loading</p>
        </div>
    ),
    ssr: false,
});

const Maps = () => {
    const router = useRouter();

    const { id } = router.query;

    const [name, setName] = useState<any>("");
    const [data, setData] = useState<any>([]);
    const [detail, setDetail] = useState<any>();

    const [message, setMessage] = useState("Memuat data...");

    // Fetching
    const getData = async () => {
        try {
            const AUTH_KEY = JSON.parse(
                localStorage.getItem("authUser") || ""
            ).token;

            const response: any = await axios.get(
                `${BASE_URL}/geolocation-log/jobId/${id}`,
                {
                    headers: {
                        Authorization: `Bearer ${AUTH_KEY}`,
                    },
                }
            );

            if (response.length > 0) {
                const res = response?.[0];

                const result = res?.data.map((_: any, index: number) =>
                    response?.[0]?.data[index].logs.map((location: any) => ({
                        lat: location.lat,
                        lng: location.lng,
                    }))
                );

                const formattedData = res.data.map((item: any) => ({
                    name: item.phoneName ?? "-",
                    phone: item.phoneId,
                    last_updated: formatDateTime(
                        item.logs.length > 0 ? item.logs[0].dateTime : null
                    ),
                }));
                setName(res.groupName);
                setDetail(formattedData);
                setData(result);
            } else {
                setMessage(
                    "Belum ada data. Anda akan kembali ke halaman sebelumnya dalam 3 detik"
                );
                setTimeout(() => {
                    router.back();
                }, 3000);
            }
        } catch (error) {
            console.log(error);
        }
    };

    useEffect(() => {
        if (id) {
            getData();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [router]);

    return data.length > 0 ? (
        <div>
            <RenderMap data={data} />

            {/* Detail */}
            <Card
                className="card-custom"
                style={{
                    position: "absolute",
                    top: 24,
                    left: 88,
                    right: 24,
                    background: "rgba(0, 0, 0, 0.1)",
                    boxShadow: "0px 0px 10px rgba(255, 255, 255, 0.4)",
                }}
            >
                <Card.Body>
                    <h5 className="text-white">{name}</h5>
                    <hr className="text-white" />
                    {detail?.map((item: any, index: number) => (
                        <div key={index}>
                            <Row>
                                <Col lg={10} s={10} xs={10}>
                                    <span className="text-white text-start">
                                        {item.name} / {item.phone}
                                    </span>
                                    <br />
                                    <span className="text-white text-start">
                                        Last updated: {item.last_updated}
                                    </span>
                                </Col>
                                <Col lg={2} s={2} xs={2}>
                                    <i className="bi bi-geo-alt-fill text-primary"></i>
                                </Col>
                            </Row>
                            <hr className="text-white" />
                        </div>
                    ))}
                </Card.Body>
            </Card>

            <Button
                variant="outline-light"
                onClick={() => router.back()}
                className="btn-icon"
                style={{
                    position: "absolute",
                    top: 24,
                    left: 24,
                    right: 24,
                    background: "rgba(0, 0, 0, 0.1)",
                    boxShadow: "0px 0px 10px rgba(255, 255, 255, 0.4)",
                    borderRadius: 999,
                }}
            >
                <i className="ri-arrow-left-line"></i>
            </Button>
        </div>
    ) : (
        <div
            style={{
                position: "absolute",
                top: "50%",
                left: "50%",
                transform: "translate(-50%, -50%)",
                textAlign: "center",
            }}
        >
            {message}
        </div>
    );
};

function getColorForIndex(index: number) {
    const colors = ["red", "blue", "green", "yellow"];
    return colors[index % colors.length];
}

export default Maps;
