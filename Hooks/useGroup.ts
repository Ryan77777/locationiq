import {
    UseMutationResult,
    UseQueryResult,
    useMutation,
    useQuery,
} from "@tanstack/react-query";
import { APIClient } from "Components/helpers/api_helper";

const API = new APIClient();

export const useGetGroups = (): UseQueryResult<any, Error> => {
    return useQuery<any, Error>({
        queryKey: ["groups"],
        queryFn: async () => {
            const response: any = await API.get("/groups");

            return response;
        },
        retry: true,
        refetchOnWindowFocus: true,
        refetchOnMount: true,
    });
};

export const useCreateGroup = (): UseMutationResult<any, Error, any> => {
    return useMutation({
        mutationFn: async (data) => {
            const response: any = await API.post("/groups", data);

            return response;
        },
    });
};

export const useUpdateGroup = (): UseMutationResult<any, Error, any> => {
    return useMutation({
        mutationFn: async (data) => {
            const response: any = await API.put("/groups", data);

            return response;
        },
    });
};

export const useDeleteGroup = (): UseMutationResult<any, Error, any> => {
    return useMutation({
        mutationFn: async (id: string) => {
            const response: any = await API.delete(`/groups/${id}`);

            return response;
        },
    });
};
