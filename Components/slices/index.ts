import {
    Action,
    AnyAction,
    combineReducers,
    configureStore,
    ThunkAction,
} from "@reduxjs/toolkit";
import { createWrapper, HYDRATE } from "next-redux-wrapper";
import { persistReducer, persistStore } from "redux-persist";
import { encryptTransform } from "redux-persist-transform-encrypt";
import createWebStorage from "redux-persist/lib/storage/createWebStorage";

// Reducer
import LoginReducer from "./auth/login/reducer";
import KTPReducer from "./ktp/reducer";
import LayoutReducer from "./layouts/reducer";
import LogsReducer from "./logs/reducer";
import SingleTargetReducer from "./single-target/reducer";
import UtilsReducer from "./utils/reducer";

const createNoopStorage = () => {
    return {
        getItem(_key: any) {
            return Promise.resolve(null);
        },
        setItem(_key: any, value: any) {
            return Promise.resolve(value);
        },
        removeItem(_key: any) {
            return Promise.resolve();
        },
    };
};

const storage =
    typeof window !== "undefined"
        ? createWebStorage("local")
        : createNoopStorage();

const persistConfig = {
    key: "root",
    storage,
    transforms: [
        encryptTransform({
            secretKey: "locationiq",
        }),
    ],
    whitelist: ["Login", "Layout"],
};

const combinedReducer = combineReducers({
    Layout: LayoutReducer,
    Login: LoginReducer,
    SingleTarget: SingleTargetReducer,
    Logs: LogsReducer,
    KTP: KTPReducer,
    Utils: UtilsReducer,
});

const reducer = (state: any, action: AnyAction) => {
    if (action.type === HYDRATE) {
        const nextState = {
            ...state,
            ...action.payload,
        };
        return nextState;
    } else {
        return combinedReducer(state, action);
    }
};

const persistedReducer = persistReducer(persistConfig, reducer);

export const store = configureStore({
    reducer: persistedReducer,
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            serializableCheck: false,
        }),
    devTools: true,
});

export const persistor = persistStore(store);

export const wrapper = createWrapper(() => store, { debug: false });

type Store = ReturnType<any>;

export type AppDispatch = Store["dispatch"];
export type RootState = ReturnType<Store["getState"]>;
export type AppThunk<ReturnType = void> = ThunkAction<
    ReturnType,
    RootState,
    unknown,
    Action<string>
>;
