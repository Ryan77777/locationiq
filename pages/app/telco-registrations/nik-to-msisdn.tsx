import Layout from "@common/Layout";
import Table from "@component/Telco/NIK/Table";
import { APIClient } from "Components/helpers/api_helper";
import Head from "next/head";
import React, { ReactElement, useCallback, useState } from "react";
import { Card, Col, Container, Form, Row } from "react-bootstrap";

const API = new APIClient();

const TelcoNIK = () => {
    const [id, setId] = useState("");

    const [isSearch, setIsSearch] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [data, setData] = useState([]);

    const onChange = (e: any) => {
        setId(e.target.value);
    };

    const searchDataByNIK = useCallback(async (id: string) => {
        setIsLoading(true);
        try {
            const response: any = await API.get(`/telko-nik/${id}`);

            setIsLoading(false);
            setIsSearch(true);

            if (response) {
                setData(response);
            }
        } catch (error) {
            console.log(error);
            setIsLoading(false);
            setIsSearch(true);
        }
    }, []);

    return (
        <React.Fragment>
            <Head>
                <title>Telco Registrations | 0320 Xplorer</title>
            </Head>
            <div className="page-content">
                <Container fluid>
                    <Row>
                        <Col lg={12}>
                            <Card>
                                <Card.Header>
                                    <h5 className="card-title mb-0">
                                        Telco Registrations (NIK to MSISDN)
                                    </h5>
                                </Card.Header>
                                <Card.Body>
                                    <Row>
                                        <Col xl={6}>
                                            <div className="mb-3">
                                                <Form.Label>
                                                    Enter your NIK
                                                </Form.Label>
                                                <Form.Control
                                                    className="form-control"
                                                    type="text"
                                                    placeholder="Input here..."
                                                    value={id}
                                                    onChange={onChange}
                                                    onKeyDown={(e) => {
                                                        if (e.key === "Enter") {
                                                            searchDataByNIK(id);
                                                        }
                                                    }}
                                                />
                                            </div>
                                        </Col>
                                    </Row>
                                    {isSearch && <Table data={data || []} />}
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>

            {isLoading && (
                <div
                    style={{
                        background: "rgba(0, 0, 0, 0.5)",
                        position: "absolute",
                        top: 0,
                        bottom: 0,
                        left: 0,
                        right: 0,
                        zIndex: 9999999,
                    }}
                >
                    <div id="status">
                        <div className="spinner-border text-primary avatar-sm">
                            <span className="visually-hidden">Loading...</span>
                        </div>
                    </div>
                </div>
            )}
        </React.Fragment>
    );
};

TelcoNIK.getLayout = (page: ReactElement) => {
    return <Layout>{page}</Layout>;
};

export default TelcoNIK;
