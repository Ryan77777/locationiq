import Layout from "@common/Layout";
import { APIClient } from "Components/helpers/api_helper";
import Head from "next/head";
import React, { ReactElement, useCallback, useState } from "react";
import { Card, Col, Container, Form, Row, Table } from "react-bootstrap";

const API = new APIClient();

const Vehicle = () => {
    const [number, setNumber] = useState("");

    const [isSearch, setIsSearch] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [data, setData] = useState<any>(null);

    const onChange = (e: any) => {
        setNumber(e.target.value);
    };

    const searchData = useCallback(async (number: string) => {
        setIsLoading(true);
        try {
            const response: any = await API.get(`/samsat-nopol/${number}`);

            setIsLoading(false);
            setIsSearch(true);

            if (response) {
                const allNull = Object.values(response).every(
                    (value) => value === null
                );

                if (allNull) {
                    setData(null);
                } else {
                    setData(response);
                }
            }
        } catch (error) {
            console.log(error);
            setIsLoading(false);
            setIsSearch(true);
            setData(null);
        }
    }, []);

    return (
        <React.Fragment>
            <Head>
                <title>Samsat | 0320 Xplorer</title>
            </Head>
            <div className="page-content">
                <Container fluid>
                    <Row>
                        <Col lg={12}>
                            <Card>
                                <Card.Header>
                                    <h5 className="card-title mb-0">
                                        Samsat (Search By Vehicle Number)
                                    </h5>
                                </Card.Header>
                                <Card.Body>
                                    <Row>
                                        <Col xl={6}>
                                            <div className="mb-3">
                                                <Form.Label>
                                                    Enter your Vehicle Number
                                                </Form.Label>
                                                <Form.Control
                                                    className="form-control"
                                                    type="text"
                                                    placeholder="Ex: A1234XYZ"
                                                    value={number}
                                                    onChange={onChange}
                                                    onKeyDown={(e) => {
                                                        if (e.key === "Enter") {
                                                            searchData(number);
                                                        }
                                                    }}
                                                />
                                            </div>
                                        </Col>
                                    </Row>
                                    {isSearch && data && (
                                        <Row>
                                            <Col xl={6}>
                                                <Table hover bordered>
                                                    <tbody>
                                                        <tr>
                                                            <th
                                                                style={{
                                                                    width: "40%",
                                                                }}
                                                            >
                                                                Nama Pemilik
                                                            </th>
                                                            <td
                                                                style={{
                                                                    width: "60%",
                                                                }}
                                                            >
                                                                {data.PEMILIK ??
                                                                    "-"}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th
                                                                style={{
                                                                    width: "40%",
                                                                }}
                                                            >
                                                                Alamat Pemilik
                                                            </th>
                                                            <td
                                                                style={{
                                                                    width: "60%",
                                                                }}
                                                            >
                                                                {data.ALAMAT ??
                                                                    "-"}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th
                                                                style={{
                                                                    width: "40%",
                                                                }}
                                                            >
                                                                NIK
                                                            </th>
                                                            <td
                                                                style={{
                                                                    width: "60%",
                                                                }}
                                                            >
                                                                {data.NIK ??
                                                                    "-"}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th
                                                                style={{
                                                                    width: "40%",
                                                                }}
                                                            >
                                                                Tipe
                                                            </th>
                                                            <td
                                                                style={{
                                                                    width: "60%",
                                                                }}
                                                            >
                                                                {data.TIPE_KEND ??
                                                                    "-"}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th
                                                                style={{
                                                                    width: "40%",
                                                                }}
                                                            >
                                                                Warna
                                                            </th>
                                                            <td
                                                                style={{
                                                                    width: "60%",
                                                                }}
                                                            >
                                                                {data.WARNA ??
                                                                    "-"}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th
                                                                style={{
                                                                    width: "40%",
                                                                }}
                                                            >
                                                                Merk
                                                            </th>
                                                            <td
                                                                style={{
                                                                    width: "60%",
                                                                }}
                                                            >
                                                                {data.MERK ??
                                                                    "-"}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th
                                                                style={{
                                                                    width: "40%",
                                                                }}
                                                            >
                                                                Model
                                                            </th>
                                                            <td
                                                                style={{
                                                                    width: "60%",
                                                                }}
                                                            >
                                                                {data.MODEL ??
                                                                    "-"}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th
                                                                style={{
                                                                    width: "40%",
                                                                }}
                                                            >
                                                                Bahan Bakar
                                                            </th>
                                                            <td
                                                                style={{
                                                                    width: "60%",
                                                                }}
                                                            >
                                                                {data.BBM ??
                                                                    "-"}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th
                                                                style={{
                                                                    width: "40%",
                                                                }}
                                                            >
                                                                Jenis
                                                            </th>
                                                            <td
                                                                style={{
                                                                    width: "60%",
                                                                }}
                                                            >
                                                                {data.JENIS_KEND ??
                                                                    "-"}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th
                                                                style={{
                                                                    width: "40%",
                                                                }}
                                                            >
                                                                Tahun
                                                            </th>
                                                            <td
                                                                style={{
                                                                    width: "60%",
                                                                }}
                                                            >
                                                                {data.RAKIT ??
                                                                    "-"}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </Table>
                                            </Col>
                                            <Col xl={6}>
                                                <Table hover bordered>
                                                    <tbody>
                                                        <tr>
                                                            <th
                                                                style={{
                                                                    width: "40%",
                                                                }}
                                                            >
                                                                No Polisi
                                                            </th>
                                                            <td
                                                                style={{
                                                                    width: "60%",
                                                                }}
                                                            >
                                                                {data.NO_POLISI ??
                                                                    "-"}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th
                                                                style={{
                                                                    width: "40%",
                                                                }}
                                                            >
                                                                No Mesin
                                                            </th>
                                                            <td
                                                                style={{
                                                                    width: "60%",
                                                                }}
                                                            >
                                                                {data.NO_MESIN ??
                                                                    "-"}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th
                                                                style={{
                                                                    width: "40%",
                                                                }}
                                                            >
                                                                No BPKB
                                                            </th>
                                                            <td
                                                                style={{
                                                                    width: "60%",
                                                                }}
                                                            >
                                                                {data.NO_BPKB ??
                                                                    "-"}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th
                                                                style={{
                                                                    width: "40%",
                                                                }}
                                                            >
                                                                No Rangka
                                                            </th>
                                                            <td
                                                                style={{
                                                                    width: "60%",
                                                                }}
                                                            >
                                                                {data.NO_RANGKA ??
                                                                    "-"}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th
                                                                style={{
                                                                    width: "40%",
                                                                }}
                                                            >
                                                                Kode Jenis
                                                            </th>
                                                            <td
                                                                style={{
                                                                    width: "60%",
                                                                }}
                                                            >
                                                                {data.KODE_JENIS ??
                                                                    "-"}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th
                                                                style={{
                                                                    width: "40%",
                                                                }}
                                                            >
                                                                Golongan
                                                            </th>
                                                            <td
                                                                style={{
                                                                    width: "60%",
                                                                }}
                                                            >
                                                                {data.GOL_KEND ??
                                                                    "-"}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th
                                                                style={{
                                                                    width: "40%",
                                                                }}
                                                            >
                                                                Tanggal Daftar
                                                            </th>
                                                            <td
                                                                style={{
                                                                    width: "60%",
                                                                }}
                                                            >
                                                                {data.TGL_DAFTAR ??
                                                                    "-"}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th
                                                                style={{
                                                                    width: "40%",
                                                                }}
                                                            >
                                                                Tanggal STNK
                                                            </th>
                                                            <td
                                                                style={{
                                                                    width: "60%",
                                                                }}
                                                            >
                                                                {data.TGL_STNK ??
                                                                    "-"}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th
                                                                style={{
                                                                    width: "40%",
                                                                }}
                                                            >
                                                                Tanggal Mati
                                                            </th>
                                                            <td
                                                                style={{
                                                                    width: "60%",
                                                                }}
                                                            >
                                                                {data.TGL_MATI_YAD ??
                                                                    "-"}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th
                                                                style={{
                                                                    width: "40%",
                                                                }}
                                                            >
                                                                Samsat Bayar
                                                            </th>
                                                            <td
                                                                style={{
                                                                    width: "60%",
                                                                }}
                                                            >
                                                                {data.SAMSAT_BAYAR ??
                                                                    "-"}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </Table>
                                            </Col>
                                        </Row>
                                    )}
                                    {isSearch && data === null && (
                                        <Row>
                                            <Col xl={12}>
                                                <Table hover bordered>
                                                    <tbody>
                                                        <tr>
                                                            <td
                                                                style={{
                                                                    width: "100%",
                                                                    textAlign:
                                                                        "center",
                                                                }}
                                                            >
                                                                No Data Found
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </Table>
                                            </Col>
                                        </Row>
                                    )}
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>

            {isLoading && (
                <div
                    style={{
                        background: "rgba(0, 0, 0, 0.5)",
                        position: "absolute",
                        top: 0,
                        bottom: 0,
                        left: 0,
                        right: 0,
                        zIndex: 9999999,
                    }}
                >
                    <div id="status">
                        <div className="spinner-border text-primary avatar-sm">
                            <span className="visually-hidden">Loading...</span>
                        </div>
                    </div>
                </div>
            )}
        </React.Fragment>
    );
};

Vehicle.getLayout = (page: ReactElement) => {
    return <Layout>{page}</Layout>;
};

export default Vehicle;
