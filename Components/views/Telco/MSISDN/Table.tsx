import TableContainer from "@common/TableContainer";
import moment from "moment";
import React, { useMemo } from "react";

const Table: React.FC<any> = ({ data }) => {
    const columns = useMemo(
        () => [
            {
                Header: "No.",
                Cell: ({ row }: any) => row.index + 1,
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "Phone Number",
                accessor: "NO_PESERTA",
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "NIK",
                accessor: "PENCARIAN",
                disableFilters: true,
                filterable: false,
            },
            {
                Header: "Registration Date",
                accessor: (cellProps: any) => {
                    return (
                        <p>
                            {moment(cellProps?.registration_data?.[0]).format(
                                "MM/DD/YYYY"
                            )}
                        </p>
                    );
                },
                disableFilters: true,
                filterable: false,
            },
        ],
        []
    );

    return (
        <React.Fragment>
            <TableContainer
                columns={columns}
                data={data}
                isPagination
                isBordered
                customPageSize={10}
                className="custom-header-css table align-middle table-nowrap"
                tableClass="table-centered align-middle table-nowrap mb-0"
                theadClass="text-muted table-light"
                SearchPlaceholder="Search..."
            />
        </React.Fragment>
    );
};

export default Table;
