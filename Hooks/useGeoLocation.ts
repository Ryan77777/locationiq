import {
    UseMutationResult,
    UseQueryResult,
    useMutation,
    useQuery,
} from "@tanstack/react-query";
import { APIClient } from "Components/helpers/api_helper";

const API = new APIClient();

export const useGetGeoLocation = (): UseQueryResult<any, Error> => {
    return useQuery<any, Error>({
        queryKey: ["geolocation"],
        queryFn: async () => {
            const response: any = await API.get("/geolocation");

            return response;
        },
        retry: true,
        refetchOnWindowFocus: true,
        refetchOnMount: true,
    });
};

export const useCreateGeoLocation = (): UseMutationResult<any, Error, any> => {
    return useMutation({
        mutationFn: async (data) => {
            const response: any = await API.post("/geolocation", data);

            return response;
        },
    });
};

export const useUpdateGeoLocation = (): UseMutationResult<any, Error, any> => {
    return useMutation({
        mutationFn: async (data) => {
            const response: any = await API.put("/geolocation", data);

            return response;
        },
    });
};

export const useDeleteGeoLocation = (): UseMutationResult<any, Error, any> => {
    return useMutation({
        mutationFn: async (id: string) => {
            const response: any = await API.delete(`/geolocation/${id}`);

            return response;
        },
    });
};
